﻿using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;

namespace MikolajMazurekLab3SQL
{
    class Grades
    {
        private static string queryGrades = "SELECT * FROM ViewAllGradesDetail;";
        private static string queryGradeStatus = "SELECT ID AS \"Lp\", STATUS AS \"Status oceny\" FROM GradeStatus;";
        private static string queryGradeTypess = "SELECT ID AS \"LP\", DESCRIPTION AS \"Opis dłuższy\", DESCRIPTION_SHORT AS \"Opis krótszy\" FROM GradeTypes;";

        public static void Show(SqlConnection sqlConnection, DataGridView dataGridView, string table)
        {
            string sqlQuery = "";
            switch (table)
            {
                case "Grades":
                    sqlQuery = queryGrades;
                    break;
                case "GradeStatus":
                    sqlQuery = queryGradeStatus;
                    break;
                case "GradeTypes":
                    sqlQuery = queryGradeTypess;
                    break;
            }
            if (sqlQuery == "")
                return;
            
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlQuery, sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
    }
}
