﻿using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MikolajMazurekLab3SQL
{
    class Student
    {
        private static string queryStudent = "SELECT ID AS \"Lp\", (Name + ' ' + Surname)  AS \"Imię i nazwisko\", BirthDate AS \"Data urodzenia\", Login, City AS \"Miasto zamieszkania\", Class AS \"Klasa\" FROM Student;";

        public static void ShowStudents(SqlConnection sqlConnection, DataGridView dataGridView)
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(queryStudent, sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }

        public static DataTable GetStudentById(SqlConnection sqlConnection, int id)
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter($"SELECT * FROM Student WHERE ID='{id}'", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);

            return dataTable;
        }

        public static void EditStudentById(SqlConnection sqlConnection, int id, string[] EditedData)
        {
            sqlConnection.Open();
            string command = $"UPDATE Student SET Name='{EditedData[0]}', Surname='{EditedData[1]}', BirthDate='{EditedData[2]}', Login='{EditedData[3]}', City='{EditedData[4]}', Class='{EditedData[5]}' WHERE ID = '{id}'";
            SqlCommand sqlCommand = new SqlCommand(command, sqlConnection);
            sqlCommand.ExecuteNonQuery();
            sqlConnection.Close();
        }

        public static void DeleteStudentById(SqlConnection sqlConnection, int id)
        {
            sqlConnection.Open();
            string command = $"DELETE FROM Student WHERE id='{id}'";
            SqlCommand sqlCommand = new SqlCommand(command, sqlConnection);
            sqlCommand.ExecuteNonQuery();
            sqlConnection.Close();
        }

        public static void AddStudent(SqlConnection sqlConnection, string[] NewStudent)
        {
            sqlConnection.Open();
            string command = $"INSERT INTO Student VALUES ('{NewStudent[0]}','{NewStudent[1]}','{NewStudent[2]}','{NewStudent[3]}','{NewStudent[4]}','{NewStudent[5]}');";
            SqlCommand sqlCommand = new SqlCommand(command, sqlConnection);
            sqlCommand.ExecuteNonQuery();
            sqlConnection.Close();
        }

        public static void SearchByName(SqlConnection sqlConnection, DataGridView dataGridView, string searchString)
        { 
            string command = $"SELECT * FROM Student  WHERE Surname+''+Name LIKE '%{searchString}%';";
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command, sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
    }
}
