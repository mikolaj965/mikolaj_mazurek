﻿using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MikolajMazurekLab3SQL
{
    class Teacher
    {
        private static string queryTeachers= "SELECT t.ID AS \"Lp\", (tt.TITLE_SHORT + ' ' + t.Name + ' ' + t.Surname)  AS \"Imię i nazwisko\", tt.Title AS \"Tytuł naukowy\", BirthDate AS \"Data urodzenia\", HireDate AS \"Data zatrudnienia\", Login, City AS \"Miasto zamieszkania\" FROM Teachers t JOIN TeacherTitle tt ON tt.ID = t.TitleID;";
        private static string queryTeacherTitle = "SELECT ID AS \"Lp\", TITLE AS \"Tytuł\", TITLE_SHORT AS \"Skrót\", DESCRIPTION AS \"Opis\" FROM TeacherTitle;";

        public static void Show(SqlConnection sqlConnection, DataGridView dataGridView, string table)
        {
            string sqlQuery = "";
            switch (table)
            {
                case "Teachers":
                    sqlQuery = queryTeachers;
                    break;
                case "TeacherTitle":
                    sqlQuery = queryTeacherTitle;
                    break;
            }
            
            if (sqlQuery == "")
                return;

            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlQuery, sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
    }
}
