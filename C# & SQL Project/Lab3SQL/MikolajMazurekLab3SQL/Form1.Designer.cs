﻿namespace MikolajMazurekLab3SQL
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewSQL = new System.Windows.Forms.DataGridView();
            this.labelTable = new System.Windows.Forms.Label();
            this.comboBoxTableView = new System.Windows.Forms.ComboBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.oDziennikuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zamknijToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelName = new System.Windows.Forms.Label();
            this.labelSurname = new System.Windows.Forms.Label();
            this.labelBirthDate = new System.Windows.Forms.Label();
            this.labelLogin = new System.Windows.Forms.Label();
            this.labelCity = new System.Windows.Forms.Label();
            this.labelClass = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxSurname = new System.Windows.Forms.TextBox();
            this.textBoxLogin = new System.Windows.Forms.TextBox();
            this.textBoxCity = new System.Windows.Forms.TextBox();
            this.radioButtonClass1 = new System.Windows.Forms.RadioButton();
            this.radioButtonClass2 = new System.Windows.Forms.RadioButton();
            this.radioButtonClass3 = new System.Windows.Forms.RadioButton();
            this.groupBoxStudent = new System.Windows.Forms.GroupBox();
            this.buttonEditCancel = new System.Windows.Forms.Button();
            this.dateTimePickerBirth = new System.Windows.Forms.DateTimePicker();
            this.textBoxEditID = new System.Windows.Forms.TextBox();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.labelEditID = new System.Windows.Forms.Label();
            this.buttonEditSave = new System.Windows.Forms.Button();
            this.buttonAddStudent = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.groupBoxSearch = new System.Windows.Forms.GroupBox();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSQL)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBoxStudent.SuspendLayout();
            this.groupBoxSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewSQL
            // 
            this.dataGridViewSQL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSQL.Location = new System.Drawing.Point(12, 59);
            this.dataGridViewSQL.Name = "dataGridViewSQL";
            this.dataGridViewSQL.Size = new System.Drawing.Size(828, 158);
            this.dataGridViewSQL.TabIndex = 0;
            // 
            // labelTable
            // 
            this.labelTable.AutoSize = true;
            this.labelTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTable.Location = new System.Drawing.Point(12, 33);
            this.labelTable.Name = "labelTable";
            this.labelTable.Size = new System.Drawing.Size(101, 16);
            this.labelTable.TabIndex = 1;
            this.labelTable.Text = "Wybierz tabelę:";
            // 
            // comboBoxTableView
            // 
            this.comboBoxTableView.FormattingEnabled = true;
            this.comboBoxTableView.Location = new System.Drawing.Point(119, 32);
            this.comboBoxTableView.Name = "comboBoxTableView";
            this.comboBoxTableView.Size = new System.Drawing.Size(315, 21);
            this.comboBoxTableView.TabIndex = 2;
            this.comboBoxTableView.SelectedIndexChanged += new System.EventHandler(this.comboBoxTableView_SelectedIndexChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.oDziennikuToolStripMenuItem,
            this.zamknijToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(852, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // oDziennikuToolStripMenuItem
            // 
            this.oDziennikuToolStripMenuItem.Name = "oDziennikuToolStripMenuItem";
            this.oDziennikuToolStripMenuItem.Size = new System.Drawing.Size(91, 20);
            this.oDziennikuToolStripMenuItem.Text = "O dzienniku...";
            this.oDziennikuToolStripMenuItem.Click += new System.EventHandler(this.oDziennikuToolStripMenuItem_Click);
            // 
            // zamknijToolStripMenuItem
            // 
            this.zamknijToolStripMenuItem.Name = "zamknijToolStripMenuItem";
            this.zamknijToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.zamknijToolStripMenuItem.Text = "Zamknij";
            this.zamknijToolStripMenuItem.Click += new System.EventHandler(this.zamknijToolStripMenuItem_Click);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(19, 26);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(29, 13);
            this.labelName.TabIndex = 4;
            this.labelName.Text = "Imię:";
            // 
            // labelSurname
            // 
            this.labelSurname.AutoSize = true;
            this.labelSurname.Location = new System.Drawing.Point(19, 53);
            this.labelSurname.Name = "labelSurname";
            this.labelSurname.Size = new System.Drawing.Size(56, 13);
            this.labelSurname.TabIndex = 5;
            this.labelSurname.Text = "Nazwisko:";
            // 
            // labelBirthDate
            // 
            this.labelBirthDate.AutoSize = true;
            this.labelBirthDate.Location = new System.Drawing.Point(19, 80);
            this.labelBirthDate.Name = "labelBirthDate";
            this.labelBirthDate.Size = new System.Drawing.Size(82, 13);
            this.labelBirthDate.TabIndex = 6;
            this.labelBirthDate.Text = "Data urodzenia:";
            // 
            // labelLogin
            // 
            this.labelLogin.AutoSize = true;
            this.labelLogin.Location = new System.Drawing.Point(19, 107);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(36, 13);
            this.labelLogin.TabIndex = 7;
            this.labelLogin.Text = "Login:";
            // 
            // labelCity
            // 
            this.labelCity.AutoSize = true;
            this.labelCity.Location = new System.Drawing.Point(19, 134);
            this.labelCity.Name = "labelCity";
            this.labelCity.Size = new System.Drawing.Size(41, 13);
            this.labelCity.TabIndex = 8;
            this.labelCity.Text = "Miasto:";
            // 
            // labelClass
            // 
            this.labelClass.AutoSize = true;
            this.labelClass.Location = new System.Drawing.Point(19, 161);
            this.labelClass.Name = "labelClass";
            this.labelClass.Size = new System.Drawing.Size(36, 13);
            this.labelClass.TabIndex = 9;
            this.labelClass.Text = "Klasa:";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(116, 23);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(100, 20);
            this.textBoxName.TabIndex = 10;
            // 
            // textBoxSurname
            // 
            this.textBoxSurname.Location = new System.Drawing.Point(116, 50);
            this.textBoxSurname.Name = "textBoxSurname";
            this.textBoxSurname.Size = new System.Drawing.Size(100, 20);
            this.textBoxSurname.TabIndex = 11;
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.Location = new System.Drawing.Point(116, 104);
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.Size = new System.Drawing.Size(100, 20);
            this.textBoxLogin.TabIndex = 13;
            // 
            // textBoxCity
            // 
            this.textBoxCity.Location = new System.Drawing.Point(116, 131);
            this.textBoxCity.Name = "textBoxCity";
            this.textBoxCity.Size = new System.Drawing.Size(100, 20);
            this.textBoxCity.TabIndex = 14;
            // 
            // radioButtonClass1
            // 
            this.radioButtonClass1.AutoSize = true;
            this.radioButtonClass1.Location = new System.Drawing.Point(116, 157);
            this.radioButtonClass1.Name = "radioButtonClass1";
            this.radioButtonClass1.Size = new System.Drawing.Size(31, 17);
            this.radioButtonClass1.TabIndex = 15;
            this.radioButtonClass1.TabStop = true;
            this.radioButtonClass1.Text = "1";
            this.radioButtonClass1.UseVisualStyleBackColor = true;
            // 
            // radioButtonClass2
            // 
            this.radioButtonClass2.AutoSize = true;
            this.radioButtonClass2.Location = new System.Drawing.Point(153, 157);
            this.radioButtonClass2.Name = "radioButtonClass2";
            this.radioButtonClass2.Size = new System.Drawing.Size(31, 17);
            this.radioButtonClass2.TabIndex = 16;
            this.radioButtonClass2.TabStop = true;
            this.radioButtonClass2.Text = "2";
            this.radioButtonClass2.UseVisualStyleBackColor = true;
            // 
            // radioButtonClass3
            // 
            this.radioButtonClass3.AutoSize = true;
            this.radioButtonClass3.Location = new System.Drawing.Point(190, 157);
            this.radioButtonClass3.Name = "radioButtonClass3";
            this.radioButtonClass3.Size = new System.Drawing.Size(31, 17);
            this.radioButtonClass3.TabIndex = 17;
            this.radioButtonClass3.TabStop = true;
            this.radioButtonClass3.Text = "3";
            this.radioButtonClass3.UseVisualStyleBackColor = true;
            // 
            // groupBoxStudent
            // 
            this.groupBoxStudent.Controls.Add(this.buttonDelete);
            this.groupBoxStudent.Controls.Add(this.buttonEditCancel);
            this.groupBoxStudent.Controls.Add(this.dateTimePickerBirth);
            this.groupBoxStudent.Controls.Add(this.textBoxEditID);
            this.groupBoxStudent.Controls.Add(this.buttonEdit);
            this.groupBoxStudent.Controls.Add(this.labelEditID);
            this.groupBoxStudent.Controls.Add(this.buttonEditSave);
            this.groupBoxStudent.Controls.Add(this.buttonAddStudent);
            this.groupBoxStudent.Controls.Add(this.labelName);
            this.groupBoxStudent.Controls.Add(this.radioButtonClass3);
            this.groupBoxStudent.Controls.Add(this.labelSurname);
            this.groupBoxStudent.Controls.Add(this.radioButtonClass2);
            this.groupBoxStudent.Controls.Add(this.labelBirthDate);
            this.groupBoxStudent.Controls.Add(this.radioButtonClass1);
            this.groupBoxStudent.Controls.Add(this.labelLogin);
            this.groupBoxStudent.Controls.Add(this.textBoxCity);
            this.groupBoxStudent.Controls.Add(this.labelCity);
            this.groupBoxStudent.Controls.Add(this.textBoxLogin);
            this.groupBoxStudent.Controls.Add(this.labelClass);
            this.groupBoxStudent.Controls.Add(this.textBoxName);
            this.groupBoxStudent.Controls.Add(this.textBoxSurname);
            this.groupBoxStudent.Location = new System.Drawing.Point(12, 235);
            this.groupBoxStudent.Name = "groupBoxStudent";
            this.groupBoxStudent.Size = new System.Drawing.Size(319, 194);
            this.groupBoxStudent.TabIndex = 18;
            this.groupBoxStudent.TabStop = false;
            this.groupBoxStudent.Text = "Modyfikacje studenta";
            // 
            // buttonEditCancel
            // 
            this.buttonEditCancel.Enabled = false;
            this.buttonEditCancel.Location = new System.Drawing.Point(268, 107);
            this.buttonEditCancel.Name = "buttonEditCancel";
            this.buttonEditCancel.Size = new System.Drawing.Size(45, 23);
            this.buttonEditCancel.TabIndex = 24;
            this.buttonEditCancel.Text = "Anuluj";
            this.buttonEditCancel.UseVisualStyleBackColor = true;
            this.buttonEditCancel.Click += new System.EventHandler(this.buttonEditCancel_Click);
            // 
            // dateTimePickerBirth
            // 
            this.dateTimePickerBirth.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerBirth.Location = new System.Drawing.Point(116, 78);
            this.dateTimePickerBirth.MaxDate = new System.DateTime(2007, 12, 31, 0, 0, 0, 0);
            this.dateTimePickerBirth.MinDate = new System.DateTime(1995, 1, 1, 0, 0, 0, 0);
            this.dateTimePickerBirth.Name = "dateTimePickerBirth";
            this.dateTimePickerBirth.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerBirth.TabIndex = 23;
            this.dateTimePickerBirth.Value = new System.DateTime(2007, 12, 31, 0, 0, 0, 0);
            // 
            // textBoxEditID
            // 
            this.textBoxEditID.Location = new System.Drawing.Point(257, 80);
            this.textBoxEditID.Name = "textBoxEditID";
            this.textBoxEditID.Size = new System.Drawing.Size(56, 20);
            this.textBoxEditID.TabIndex = 22;
            // 
            // buttonEdit
            // 
            this.buttonEdit.Location = new System.Drawing.Point(222, 106);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(45, 23);
            this.buttonEdit.TabIndex = 21;
            this.buttonEdit.Text = "Edytuj";
            this.buttonEdit.UseVisualStyleBackColor = true;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // labelEditID
            // 
            this.labelEditID.AutoSize = true;
            this.labelEditID.Location = new System.Drawing.Point(222, 85);
            this.labelEditID.Name = "labelEditID";
            this.labelEditID.Size = new System.Drawing.Size(21, 13);
            this.labelEditID.TabIndex = 20;
            this.labelEditID.Text = "ID:";
            // 
            // buttonEditSave
            // 
            this.buttonEditSave.Enabled = false;
            this.buttonEditSave.Location = new System.Drawing.Point(222, 130);
            this.buttonEditSave.Name = "buttonEditSave";
            this.buttonEditSave.Size = new System.Drawing.Size(91, 23);
            this.buttonEditSave.TabIndex = 19;
            this.buttonEditSave.Text = "Zapisz edycję";
            this.buttonEditSave.UseVisualStyleBackColor = true;
            this.buttonEditSave.Click += new System.EventHandler(this.buttonEditSave_Click);
            // 
            // buttonAddStudent
            // 
            this.buttonAddStudent.Location = new System.Drawing.Point(222, 23);
            this.buttonAddStudent.Name = "buttonAddStudent";
            this.buttonAddStudent.Size = new System.Drawing.Size(91, 23);
            this.buttonAddStudent.TabIndex = 18;
            this.buttonAddStudent.Text = "Dodaj studenta";
            this.buttonAddStudent.UseVisualStyleBackColor = true;
            this.buttonAddStudent.Click += new System.EventHandler(this.buttonAddStudent_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(222, 50);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(91, 23);
            this.buttonDelete.TabIndex = 25;
            this.buttonDelete.Text = "Usuń rekord";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // groupBoxSearch
            // 
            this.groupBoxSearch.Controls.Add(this.textBoxSearch);
            this.groupBoxSearch.Location = new System.Drawing.Point(346, 235);
            this.groupBoxSearch.Name = "groupBoxSearch";
            this.groupBoxSearch.Size = new System.Drawing.Size(494, 46);
            this.groupBoxSearch.TabIndex = 26;
            this.groupBoxSearch.TabStop = false;
            this.groupBoxSearch.Text = "Wyszukiwanie w imieniu i nazwisku podanej frazy";
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.Location = new System.Drawing.Point(6, 19);
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(482, 20);
            this.textBoxSearch.TabIndex = 11;
            this.textBoxSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBoxSearch_KeyUp);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 525);
            this.Controls.Add(this.groupBoxSearch);
            this.Controls.Add(this.groupBoxStudent);
            this.Controls.Add(this.comboBoxTableView);
            this.Controls.Add(this.labelTable);
            this.Controls.Add(this.dataGridViewSQL);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Dziennik elektroniczny";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSQL)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBoxStudent.ResumeLayout(false);
            this.groupBoxStudent.PerformLayout();
            this.groupBoxSearch.ResumeLayout(false);
            this.groupBoxSearch.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewSQL;
        private System.Windows.Forms.Label labelTable;
        private System.Windows.Forms.ComboBox comboBoxTableView;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem oDziennikuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zamknijToolStripMenuItem;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelSurname;
        private System.Windows.Forms.Label labelBirthDate;
        private System.Windows.Forms.Label labelLogin;
        private System.Windows.Forms.Label labelCity;
        private System.Windows.Forms.Label labelClass;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxSurname;
        private System.Windows.Forms.TextBox textBoxLogin;
        private System.Windows.Forms.TextBox textBoxCity;
        private System.Windows.Forms.RadioButton radioButtonClass1;
        private System.Windows.Forms.RadioButton radioButtonClass2;
        private System.Windows.Forms.RadioButton radioButtonClass3;
        private System.Windows.Forms.GroupBox groupBoxStudent;
        private System.Windows.Forms.TextBox textBoxEditID;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Label labelEditID;
        private System.Windows.Forms.Button buttonEditSave;
        private System.Windows.Forms.Button buttonAddStudent;
        private System.Windows.Forms.DateTimePicker dateTimePickerBirth;
        private System.Windows.Forms.Button buttonEditCancel;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.GroupBox groupBoxSearch;
        private System.Windows.Forms.TextBox textBoxSearch;
    }
}

