﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MikolajMazurekLab3SQL
{
    public partial class Form1 : Form
    {
        SqlConnection sqlConnection;

        public Form1()
        {
            InitializeComponent();
            InitializeComboBox();
            sqlConnection = new SqlConnection("Data Source=NOTEBOOK\\SQLEXPRESS; database=Lab3; Trusted_Connection=yes");
        }

        private void InitializeComboBox()
        {
            comboBoxTableView.Items.Add("Wszystkie oceny (widok ViewAllGradesDetail)");
            comboBoxTableView.Items.Add("Statusy ocen (GradeStatus)");
            comboBoxTableView.Items.Add("Typy ocen (GradeTypes)");
            comboBoxTableView.Items.Add("Uczniowie (Student)");
            comboBoxTableView.Items.Add("Nauczyciele (Teachers)");
            comboBoxTableView.Items.Add("Tytuły naukowe nauczycieli (TeacherTitle)");
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void comboBoxTableView_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBoxTableView.SelectedIndex)
            {
                case 0:
                    Grades.Show(sqlConnection, dataGridViewSQL, "Grades");
                    break;
                case 1:
                    Grades.Show(sqlConnection, dataGridViewSQL, "GradeStatus");
                    break;
                case 2:
                    Grades.Show(sqlConnection, dataGridViewSQL, "GradeTypes");
                    break;
                case 3:
                    Student.ShowStudents(sqlConnection, dataGridViewSQL);
                    break;
                case 4:
                    Teacher.Show(sqlConnection, dataGridViewSQL, "Teachers");
                    break;
                case 5:
                    Teacher.Show(sqlConnection, dataGridViewSQL, "TeacherTitle");
                    break;
            }
        }

        private void zamknijToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void oDziennikuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Autor: Mikołaj Mazurek");
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            int id;

            if (textBoxEditID.Text == "")
            {
                MessageBox.Show("Nie podałeś ID Studenta do etytowania");
                return;
            }
            try
            {
                id = int.Parse(textBoxEditID.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Podana wartość nie jest typu int");
                return;
            }

            DataTable dataTableEdit = Student.GetStudentById(sqlConnection,id);

            if(dataTableEdit.Rows.Count == 0)
            {
                MessageBox.Show("Nie znaleziono takiego studenta, niepoprawne id");
                return;
            }
            buttonEdit.Enabled = false;
            buttonEditCancel.Enabled = true;
            buttonEditSave.Enabled = true;
            textBoxEditID.Enabled = false;

            foreach (DataRow dr in dataTableEdit.Rows)
            {
                textBoxName.Text = dr.Field<string>("NAME");
                textBoxSurname.Text = dr.Field<string>("SURNAME");
                dateTimePickerBirth.Text = dr.Field<DateTime>("BIRTHDATE").ToString();
                textBoxLogin.Text = dr.Field<string>("LOGIN");
                textBoxCity.Text = dr.Field<string>("CITY");

                if (dr.Field<int>("CLASS") == 1)
                {
                    radioButtonClass1.Checked = true;
                    radioButtonClass2.Checked = false;
                    radioButtonClass3.Checked = false;
                }
                else if (dr.Field<int>("CLASS") == 2)
                {
                    radioButtonClass1.Checked = false;
                    radioButtonClass2.Checked = true;
                    radioButtonClass3.Checked = false;
                }
                else if (dr.Field<int>("CLASS") == 3)
                {
                    radioButtonClass1.Checked = false;
                    radioButtonClass2.Checked = false;
                    radioButtonClass3.Checked = true;
                }
            }
            Student.ShowStudents(sqlConnection, dataGridViewSQL);
        }

        private void buttonEditCancel_Click(object sender, EventArgs e)
        {
            ClearEditFields();

            buttonEdit.Enabled = true;
            buttonEditCancel.Enabled = false;
            buttonEditSave.Enabled = false;
            textBoxEditID.Enabled = true;
            textBoxEditID.Text = "";
        }

        private void ClearEditFields()
        {
            foreach (Control control in groupBoxStudent.Controls)
            {
                if(control.GetType().Name == "TextBox" || control.GetType().Name == "DateTimePicker")
                    control.Text = "";
            }
        }

        private void buttonEditSave_Click(object sender, EventArgs e)
        {
            int id, selectedClass = 0;

            try
            {
                id = int.Parse(textBoxEditID.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Podana wartość nie jest typu int");
                return;
            }
            if (!radioButtonClass1.Checked && !radioButtonClass2.Checked && !radioButtonClass3.Checked)
            {
                MessageBox.Show("Nie zaznaczono wyboru klasy");
                return;
            }
            if (radioButtonClass1.Checked)
                selectedClass = 1;
            else if (radioButtonClass2.Checked)
                selectedClass = 2;
            else selectedClass = 3;

            string[] EditedData = new string[]{
                textBoxName.Text, textBoxSurname.Text,dateTimePickerBirth.Text, textBoxLogin.Text, textBoxCity.Text, selectedClass.ToString() };
            Student.EditStudentById(sqlConnection, id, EditedData);
            Student.ShowStudents(sqlConnection, dataGridViewSQL);

            buttonEdit.Enabled = true;
            buttonEditCancel.Enabled = false;
            buttonEditSave.Enabled = false;
            textBoxEditID.Enabled = true;
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            int id;

            try
            {
                id = int.Parse(textBoxEditID.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Podana wartość nie jest typu int");
                return;
            }
            Student.DeleteStudentById(sqlConnection, id);
            Student.ShowStudents(sqlConnection, dataGridViewSQL);
        }

        private void buttonAddStudent_Click(object sender, EventArgs e)
        {
            int selectedClass = 0;
            if (!radioButtonClass1.Checked && !radioButtonClass2.Checked && !radioButtonClass3.Checked)
            {
                MessageBox.Show("Nie zaznaczono wyboru klasy");
                return;
            }
            if (radioButtonClass1.Checked)
                selectedClass = 1;
            else if (radioButtonClass2.Checked)
                selectedClass = 2;
            else selectedClass = 3;

            string[] NewStudent = new string[]{
                textBoxName.Text, textBoxSurname.Text, dateTimePickerBirth.Text,textBoxLogin.Text,textBoxCity.Text, selectedClass.ToString() };
            Student.AddStudent(sqlConnection, NewStudent);
            Student.ShowStudents(sqlConnection, dataGridViewSQL);
            ClearEditFields();
        }

        private void textBoxSearch_KeyUp(object sender, KeyEventArgs e)
        {
            Student.SearchByName(sqlConnection, dataGridViewSQL, textBoxSearch.Text);
        }
    }
}
