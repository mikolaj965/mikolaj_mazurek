USE [master]
GO
/****** Object:  Database [Lab3]    Script Date: 2017-04-19 23:44:59 ******/
CREATE DATABASE [Lab3] ON  PRIMARY 
( NAME = N'Lab3', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\Lab3.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Lab3_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\Lab3_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Lab3] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Lab3].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Lab3] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Lab3] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Lab3] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Lab3] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Lab3] SET ARITHABORT OFF 
GO
ALTER DATABASE [Lab3] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Lab3] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Lab3] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Lab3] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Lab3] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Lab3] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Lab3] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Lab3] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Lab3] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Lab3] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Lab3] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Lab3] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Lab3] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Lab3] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Lab3] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Lab3] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Lab3] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Lab3] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Lab3] SET  MULTI_USER 
GO
ALTER DATABASE [Lab3] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Lab3] SET DB_CHAINING OFF 
GO
USE [Lab3]
GO
/****** Object:  Table [dbo].[Grades]    Script Date: 2017-04-19 23:44:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Grades](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GRADE_TYPE_ID] [int] NOT NULL,
	[GRADE_STUDENT_ID] [int] NOT NULL,
	[GRADE_TEACHER_ID] [int] NOT NULL,
	[GRADE_STATUS_ID] [int] NOT NULL,
	[VALUE] [int] NOT NULL,
	[DESCRIPTION] [varchar](250) NULL,
	[DESCRIPTION_SHORT] [varchar](50) NULL,
	[DATE] [date] NOT NULL,
 CONSTRAINT [PK_Grades] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GradeStatus]    Script Date: 2017-04-19 23:44:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GradeStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[STATUS] [varchar](50) NULL,
 CONSTRAINT [PK_GradeStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GradeTypes]    Script Date: 2017-04-19 23:44:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GradeTypes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DESCRIPTION] [varchar](250) NULL,
	[DESCRIPTION_SHORT] [varchar](50) NULL,
 CONSTRAINT [PK_GradeTypes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Student]    Script Date: 2017-04-19 23:44:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Student](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Surname] [varchar](50) NOT NULL,
	[BirthDate] [date] NOT NULL,
	[Login] [varchar](50) NOT NULL,
	[City] [varchar](50) NOT NULL,
	[Class] [int] NULL,
 CONSTRAINT [PK_Student] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Teachers]    Script Date: 2017-04-19 23:44:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Teachers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Surname] [varchar](50) NOT NULL,
	[TitleID] [int] NOT NULL,
	[BirthDate] [date] NOT NULL,
	[HireDate] [date] NOT NULL,
	[Login] [varchar](50) NOT NULL,
	[City] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Teachers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TeacherTitle]    Script Date: 2017-04-19 23:44:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TeacherTitle](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TITLE] [varchar](50) NOT NULL,
	[TITLE_SHORT] [varchar](10) NULL,
	[DESCRIPTION] [varchar](50) NULL,
 CONSTRAINT [PK_TeacherTitle] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[ViewAllGradesDetail]    Script Date: 2017-04-19 23:44:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewAllGradesDetail]
AS
SELECT        g.ID AS Lp, s.Name + ' ' + s.Surname AS [Imię i nazwisko studenta], g.VALUE AS [OCENA STUDENTA], g.DESCRIPTION AS [Opis oceny], 
                         g.DATE AS [Data otrzymania ocey], gt.DESCRIPTION AS [Typ oceny], gs.STATUS AS [Status oceny], 
                         tt.TITLE_SHORT + ' ' + t.Name + ' ' + t.Surname AS [Imię i nazwisko nauczyciela]
FROM            dbo.Grades AS g INNER JOIN
                         dbo.GradeTypes AS gt ON g.GRADE_TYPE_ID = gt.ID INNER JOIN
                         dbo.Student AS s ON s.ID = g.GRADE_STUDENT_ID INNER JOIN
                         dbo.Teachers AS t ON t.ID = g.GRADE_TEACHER_ID INNER JOIN
                         dbo.GradeStatus AS gs ON gs.ID = g.GRADE_STATUS_ID INNER JOIN
                         dbo.TeacherTitle AS tt ON tt.ID = t.TitleID

GO
SET IDENTITY_INSERT [dbo].[Grades] ON 

INSERT [dbo].[Grades] ([ID], [GRADE_TYPE_ID], [GRADE_STUDENT_ID], [GRADE_TEACHER_ID], [GRADE_STATUS_ID], [VALUE], [DESCRIPTION], [DESCRIPTION_SHORT], [DATE]) VALUES (2, 1, 1, 2, 1, 5, N'Zadanie wykonane popawnie, bardzo dobrze, należy się dobra ocena', NULL, CAST(N'2017-04-17' AS Date))
INSERT [dbo].[Grades] ([ID], [GRADE_TYPE_ID], [GRADE_STUDENT_ID], [GRADE_TEACHER_ID], [GRADE_STATUS_ID], [VALUE], [DESCRIPTION], [DESCRIPTION_SHORT], [DATE]) VALUES (6, 4, 1, 3, 2, 4, N'Jakiś opis wstawionej oceny', NULL, CAST(N'2017-04-17' AS Date))
SET IDENTITY_INSERT [dbo].[Grades] OFF
SET IDENTITY_INSERT [dbo].[GradeStatus] ON 

INSERT [dbo].[GradeStatus] ([ID], [STATUS]) VALUES (1, N'Otrzymana')
INSERT [dbo].[GradeStatus] ([ID], [STATUS]) VALUES (2, N'Poprawiona')
INSERT [dbo].[GradeStatus] ([ID], [STATUS]) VALUES (3, N'Anulowana')
SET IDENTITY_INSERT [dbo].[GradeStatus] OFF
SET IDENTITY_INSERT [dbo].[GradeTypes] ON 

INSERT [dbo].[GradeTypes] ([ID], [DESCRIPTION], [DESCRIPTION_SHORT]) VALUES (1, N'Sprawdzian', N'Spr')
INSERT [dbo].[GradeTypes] ([ID], [DESCRIPTION], [DESCRIPTION_SHORT]) VALUES (2, N'Kartkówka', N'Kart')
INSERT [dbo].[GradeTypes] ([ID], [DESCRIPTION], [DESCRIPTION_SHORT]) VALUES (3, N'Odpowiedź ustna ', N'Odp')
INSERT [dbo].[GradeTypes] ([ID], [DESCRIPTION], [DESCRIPTION_SHORT]) VALUES (4, N'Egzamin', N'Egz')
INSERT [dbo].[GradeTypes] ([ID], [DESCRIPTION], [DESCRIPTION_SHORT]) VALUES (5, N'Praca domowa', N'Pd')
INSERT [dbo].[GradeTypes] ([ID], [DESCRIPTION], [DESCRIPTION_SHORT]) VALUES (6, N'Aktywność na lekcji', N'Akt')
SET IDENTITY_INSERT [dbo].[GradeTypes] OFF
SET IDENTITY_INSERT [dbo].[Student] ON 

INSERT [dbo].[Student] ([ID], [Name], [Surname], [BirthDate], [Login], [City], [Class]) VALUES (1, N'Kasia', N'Kowalska', CAST(N'1996-12-19' AS Date), N'kkowalska', N'Wrocław', 2)
INSERT [dbo].[Student] ([ID], [Name], [Surname], [BirthDate], [Login], [City], [Class]) VALUES (5, N'Mikołaj', N'Testowy', CAST(N'2004-12-31' AS Date), N'mmm', N'Warszawa', 3)
INSERT [dbo].[Student] ([ID], [Name], [Surname], [BirthDate], [Login], [City], [Class]) VALUES (6, N'aaa', N'sss', CAST(N'2007-05-28' AS Date), N'asdasd', N'asdasdasdasd', 2)
SET IDENTITY_INSERT [dbo].[Student] OFF
SET IDENTITY_INSERT [dbo].[Teachers] ON 

INSERT [dbo].[Teachers] ([ID], [Name], [Surname], [TitleID], [BirthDate], [HireDate], [Login], [City]) VALUES (2, N'Jan', N'Kowalski', 1, CAST(N'1955-11-11' AS Date), CAST(N'2011-10-05' AS Date), N'jkowalski', N'Wrocław')
INSERT [dbo].[Teachers] ([ID], [Name], [Surname], [TitleID], [BirthDate], [HireDate], [Login], [City]) VALUES (3, N'Marcin', N'Adamski', 2, CAST(N'1966-01-01' AS Date), CAST(N'2010-11-23' AS Date), N'adammm', N'Lubin')
SET IDENTITY_INSERT [dbo].[Teachers] OFF
SET IDENTITY_INSERT [dbo].[TeacherTitle] ON 

INSERT [dbo].[TeacherTitle] ([ID], [TITLE], [TITLE_SHORT], [DESCRIPTION]) VALUES (1, N'Inżynier', N'inż', N'Tytuł naukowy inżynier')
INSERT [dbo].[TeacherTitle] ([ID], [TITLE], [TITLE_SHORT], [DESCRIPTION]) VALUES (2, N'Magister', N'mgr', N'Tytuł naukowy magister')
INSERT [dbo].[TeacherTitle] ([ID], [TITLE], [TITLE_SHORT], [DESCRIPTION]) VALUES (3, N'Doktor', N'dr', N'Dr')
SET IDENTITY_INSERT [dbo].[TeacherTitle] OFF
ALTER TABLE [dbo].[Grades]  WITH CHECK ADD  CONSTRAINT [FK_Grades_GradeStatus] FOREIGN KEY([GRADE_STATUS_ID])
REFERENCES [dbo].[GradeStatus] ([ID])
GO
ALTER TABLE [dbo].[Grades] CHECK CONSTRAINT [FK_Grades_GradeStatus]
GO
ALTER TABLE [dbo].[Grades]  WITH CHECK ADD  CONSTRAINT [FK_Grades_GradeTypes] FOREIGN KEY([GRADE_TYPE_ID])
REFERENCES [dbo].[GradeTypes] ([ID])
GO
ALTER TABLE [dbo].[Grades] CHECK CONSTRAINT [FK_Grades_GradeTypes]
GO
ALTER TABLE [dbo].[Grades]  WITH CHECK ADD  CONSTRAINT [FK_Grades_Student] FOREIGN KEY([GRADE_STUDENT_ID])
REFERENCES [dbo].[Student] ([ID])
GO
ALTER TABLE [dbo].[Grades] CHECK CONSTRAINT [FK_Grades_Student]
GO
ALTER TABLE [dbo].[Grades]  WITH CHECK ADD  CONSTRAINT [FK_Grades_Teachers] FOREIGN KEY([GRADE_TEACHER_ID])
REFERENCES [dbo].[Teachers] ([ID])
GO
ALTER TABLE [dbo].[Grades] CHECK CONSTRAINT [FK_Grades_Teachers]
GO
ALTER TABLE [dbo].[Teachers]  WITH CHECK ADD  CONSTRAINT [FK_Teachers_TeacherTitle] FOREIGN KEY([TitleID])
REFERENCES [dbo].[TeacherTitle] ([ID])
GO
ALTER TABLE [dbo].[Teachers] CHECK CONSTRAINT [FK_Teachers_TeacherTitle]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "g"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 243
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gt"
            Begin Extent = 
               Top = 6
               Left = 281
               Bottom = 118
               Right = 486
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 6
               Left = 524
               Bottom = 135
               Right = 694
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t"
            Begin Extent = 
               Top = 6
               Left = 732
               Bottom = 135
               Right = 902
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gs"
            Begin Extent = 
               Top = 120
               Left = 281
               Bottom = 215
               Right = 451
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tt"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 267
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
      ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewAllGradesDetail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'   Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewAllGradesDetail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewAllGradesDetail'
GO
USE [master]
GO
ALTER DATABASE [Lab3] SET  READ_WRITE 
GO
