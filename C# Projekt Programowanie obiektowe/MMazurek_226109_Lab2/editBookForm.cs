﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMazurek_226109_Lab2
{
    public partial class editBookForm : Form
    {
        public editBookForm()
        {
            InitializeComponent();
        }

        private Position position = null;

        public string TitleTextBox
        {
            get
            {
                return BookTitle.Text;
            }
            set
            {
                BookTitle.Text = value;
            }
        }

        public int DateTextBox
        {
            get
            {
                return Convert.ToInt16(BookDate.Text);
            }
            set
            {
                BookDate.Text = value.ToString();
            }
        }

        public int PagesTextBox
        {
            get
            {
                return Convert.ToInt16(BookPages.Text);
            }
            set
            {
                BookPages.Text = value.ToString();
            }
        }

        public bool CoverRadio2
        {
            get
            {
                return BookCover2.Checked;
            }
            set
            {
                BookCover2.Select();
            }
        }

        public bool CoverRadio1
        {
            get
            {
                return BookCover1.Checked;
            }
            set
            {
                BookCover1.Select();
            }
        }

        public string AuthorTextBox
        {
            get
            {
                return BookAuthor.Text;
            }
            set
            {
                BookAuthor.Text = value;
            }
        }

        public editBookForm(object sender, Position obj)
        {
            position = obj;
            InitializeComponent();
        }

        private void editForm_Load(object sender, EventArgs e)
        {
            
        }

        private void Save_Click(object sender, EventArgs e)
        {
            Book book = (Book)position;
            book.SavePosition();
            this.Close();
        }

        public void editForm_FormClosed(object sender, FormClosedEventArgs e)
        { 
        }

        private void CancelBookButton_Click(object sender, EventArgs e)
        {

            this.Close();
        }
    }
}
