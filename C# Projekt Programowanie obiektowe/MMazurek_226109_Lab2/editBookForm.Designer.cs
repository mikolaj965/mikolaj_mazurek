﻿namespace MMazurek_226109_Lab2
{
    partial class editBookForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BookTitle = new System.Windows.Forms.TextBox();
            this.Save = new System.Windows.Forms.Button();
            this.BookAuthor = new System.Windows.Forms.TextBox();
            this.BookCover2 = new System.Windows.Forms.RadioButton();
            this.BookCover1 = new System.Windows.Forms.RadioButton();
            this.BookCover = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.CancelBookButton = new System.Windows.Forms.Button();
            this.BookDate = new System.Windows.Forms.NumericUpDown();
            this.BookPages = new System.Windows.Forms.NumericUpDown();
            this.BookCover.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BookDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BookPages)).BeginInit();
            this.SuspendLayout();
            // 
            // BookTitle
            // 
            this.BookTitle.Location = new System.Drawing.Point(92, 12);
            this.BookTitle.Name = "BookTitle";
            this.BookTitle.Size = new System.Drawing.Size(120, 20);
            this.BookTitle.TabIndex = 0;
            // 
            // Save
            // 
            this.Save.Location = new System.Drawing.Point(254, 122);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(75, 23);
            this.Save.TabIndex = 1;
            this.Save.Text = "Zapisz";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // BookAuthor
            // 
            this.BookAuthor.Location = new System.Drawing.Point(92, 64);
            this.BookAuthor.Name = "BookAuthor";
            this.BookAuthor.Size = new System.Drawing.Size(120, 20);
            this.BookAuthor.TabIndex = 4;
            // 
            // BookCover2
            // 
            this.BookCover2.AutoSize = true;
            this.BookCover2.Location = new System.Drawing.Point(12, 42);
            this.BookCover2.Name = "BookCover2";
            this.BookCover2.Size = new System.Drawing.Size(61, 17);
            this.BookCover2.TabIndex = 5;
            this.BookCover2.TabStop = true;
            this.BookCover2.Text = "Twarda";
            this.BookCover2.UseVisualStyleBackColor = true;
            // 
            // BookCover1
            // 
            this.BookCover1.AutoSize = true;
            this.BookCover1.Location = new System.Drawing.Point(12, 19);
            this.BookCover1.Name = "BookCover1";
            this.BookCover1.Size = new System.Drawing.Size(60, 17);
            this.BookCover1.TabIndex = 6;
            this.BookCover1.TabStop = true;
            this.BookCover1.Text = "Miękka";
            this.BookCover1.UseVisualStyleBackColor = true;
            // 
            // BookCover
            // 
            this.BookCover.Controls.Add(this.BookCover2);
            this.BookCover.Controls.Add(this.BookCover1);
            this.BookCover.Location = new System.Drawing.Point(218, 12);
            this.BookCover.Name = "BookCover";
            this.BookCover.Size = new System.Drawing.Size(111, 98);
            this.BookCover.TabIndex = 7;
            this.BookCover.TabStop = false;
            this.BookCover.Text = "Okładka książki";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Tytuł:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Rok wydania:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Ilość stron:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Autor:";
            // 
            // CancelBookButton
            // 
            this.CancelBookButton.Location = new System.Drawing.Point(173, 122);
            this.CancelBookButton.Name = "CancelBookButton";
            this.CancelBookButton.Size = new System.Drawing.Size(75, 23);
            this.CancelBookButton.TabIndex = 12;
            this.CancelBookButton.Text = "Anuluj";
            this.CancelBookButton.UseVisualStyleBackColor = true;
            this.CancelBookButton.Click += new System.EventHandler(this.CancelBookButton_Click);
            // 
            // BookDate
            // 
            this.BookDate.Location = new System.Drawing.Point(92, 38);
            this.BookDate.Maximum = new decimal(new int[] {
            2016,
            0,
            0,
            0});
            this.BookDate.Name = "BookDate";
            this.BookDate.Size = new System.Drawing.Size(120, 20);
            this.BookDate.TabIndex = 13;
            // 
            // BookPages
            // 
            this.BookPages.Location = new System.Drawing.Point(92, 91);
            this.BookPages.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.BookPages.Name = "BookPages";
            this.BookPages.Size = new System.Drawing.Size(120, 20);
            this.BookPages.TabIndex = 14;
            // 
            // editBookForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 157);
            this.ControlBox = false;
            this.Controls.Add(this.BookPages);
            this.Controls.Add(this.BookDate);
            this.Controls.Add(this.CancelBookButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BookCover);
            this.Controls.Add(this.BookAuthor);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.BookTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "editBookForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edycja pozycji";
            this.TopMost = true;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.editForm_FormClosed);
            this.Load += new System.EventHandler(this.editForm_Load);
            this.BookCover.ResumeLayout(false);
            this.BookCover.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BookDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BookPages)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox BookTitle;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.TextBox BookAuthor;
        private System.Windows.Forms.RadioButton BookCover2;
        private System.Windows.Forms.RadioButton BookCover1;
        private System.Windows.Forms.GroupBox BookCover;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button CancelBookButton;
        private System.Windows.Forms.NumericUpDown BookDate;
        private System.Windows.Forms.NumericUpDown BookPages;
    }
}