﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace MMazurek_226109_Lab2
{
    class User
    {
        protected int id, date, books;
        protected bool gender, type;
        protected string name, surname;

        public virtual void showPosition(ListView obj)
        {
        }

        public virtual void generateXML(XmlTextWriter writer)
        {
        }

        public int getUserID()
        {
            return id;
        }

        public User AddUser(ListView list)
        {
            AddUserForm a = new AddUserForm();
            a.ShowDialog();
            if(a.Save)
            if (a.UserTypeRadio1)
            {
                ListViewItem item = new ListViewItem(Library.getNextUserID().ToString());
                item.SubItems.Add("Student");
                item.SubItems.Add("-");
                item.SubItems.Add(a.NameTextBox);
                item.SubItems.Add(a.SurnameTextBox);
                item.SubItems.Add(a.DateTextBox.ToString());
                item.SubItems.Add(((a.GenderTypeRadio2 == false) ? "M" : "K"));
                item.SubItems.Add(a.AlbumTextBox.ToString());
                item.SubItems.Add("-");
                list.Items.Add(item);
                Student obj = new Student(Library.getNextUserID(),false,a.NameTextBox,a.SurnameTextBox,a.DateTextBox,a.GenderTypeRadio2, a.AlbumTextBox,0);

                Library.enlargeUserID();
                return obj;
            }
            else if (a.UserTypeRadio2)
            {
                ListViewItem item = new ListViewItem(Library.getNextID().ToString());
                item.SubItems.Add("Nauczyciel");
                item.SubItems.Add(a.TitleTextBox);
                item.SubItems.Add(a.NameTextBox);
                item.SubItems.Add(a.SurnameTextBox);
                item.SubItems.Add(a.DateTextBox.ToString());
                item.SubItems.Add(((a.GenderTypeRadio2 == false) ? "M" : "K"));
                item.SubItems.Add("-");
                item.SubItems.Add(a.StartWorkTextBox);
                list.Items.Add(item);
                Teacher obj = new Teacher(Library.getNextID(),true, a.NameTextBox, a.SurnameTextBox, a.DateTextBox,a.GenderTypeRadio2, 0,a.StartWorkTextBox, a.TitleTextBox);

                Library.enlargeID();
                return obj;
            }
            
            return new User();
        }

        public string GetUserLogin()
        {
            return (name + "_" + surname);
        }

        public bool GetUserType()
        {
            return type;
        }
    }
}
