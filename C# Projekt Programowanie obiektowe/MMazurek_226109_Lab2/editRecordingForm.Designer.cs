﻿namespace MMazurek_226109_Lab2
{
    partial class editRecordingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CancelBookButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.RecordingExt = new System.Windows.Forms.TextBox();
            this.RecordingFileName = new System.Windows.Forms.TextBox();
            this.Save = new System.Windows.Forms.Button();
            this.RecordingTitle = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.RecordingDate = new System.Windows.Forms.NumericUpDown();
            this.RecordingLength = new System.Windows.Forms.NumericUpDown();
            this.RecordingSize = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.RecordingDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecordingLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecordingSize)).BeginInit();
            this.SuspendLayout();
            // 
            // CancelBookButton
            // 
            this.CancelBookButton.Location = new System.Drawing.Point(12, 162);
            this.CancelBookButton.Name = "CancelBookButton";
            this.CancelBookButton.Size = new System.Drawing.Size(75, 23);
            this.CancelBookButton.TabIndex = 23;
            this.CancelBookButton.Text = "Anuluj";
            this.CancelBookButton.UseVisualStyleBackColor = true;
            this.CancelBookButton.Click += new System.EventHandler(this.CancelBookButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Rozszerzenie:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Nazwa pliku:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Data:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Tytuł:";
            // 
            // RecordingExt
            // 
            this.RecordingExt.Location = new System.Drawing.Point(91, 59);
            this.RecordingExt.Name = "RecordingExt";
            this.RecordingExt.Size = new System.Drawing.Size(100, 20);
            this.RecordingExt.TabIndex = 17;
            // 
            // RecordingFileName
            // 
            this.RecordingFileName.Location = new System.Drawing.Point(91, 85);
            this.RecordingFileName.Name = "RecordingFileName";
            this.RecordingFileName.Size = new System.Drawing.Size(100, 20);
            this.RecordingFileName.TabIndex = 16;
            // 
            // Save
            // 
            this.Save.Location = new System.Drawing.Point(118, 162);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(75, 23);
            this.Save.TabIndex = 14;
            this.Save.Text = "Zapisz";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // RecordingTitle
            // 
            this.RecordingTitle.Location = new System.Drawing.Point(91, 7);
            this.RecordingTitle.Name = "RecordingTitle";
            this.RecordingTitle.Size = new System.Drawing.Size(100, 20);
            this.RecordingTitle.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 114);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "Dlugość(min):";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "Rozmiar[MB]:";
            // 
            // RecordingDate
            // 
            this.RecordingDate.Location = new System.Drawing.Point(91, 34);
            this.RecordingDate.Maximum = new decimal(new int[] {
            2016,
            0,
            0,
            0});
            this.RecordingDate.Minimum = new decimal(new int[] {
            1900,
            0,
            0,
            0});
            this.RecordingDate.Name = "RecordingDate";
            this.RecordingDate.Size = new System.Drawing.Size(100, 20);
            this.RecordingDate.TabIndex = 68;
            this.RecordingDate.Value = new decimal(new int[] {
            1900,
            0,
            0,
            0});
            // 
            // RecordingLength
            // 
            this.RecordingLength.DecimalPlaces = 2;
            this.RecordingLength.Location = new System.Drawing.Point(91, 111);
            this.RecordingLength.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.RecordingLength.Name = "RecordingLength";
            this.RecordingLength.Size = new System.Drawing.Size(100, 20);
            this.RecordingLength.TabIndex = 71;
            // 
            // RecordingSize
            // 
            this.RecordingSize.DecimalPlaces = 3;
            this.RecordingSize.Location = new System.Drawing.Point(91, 137);
            this.RecordingSize.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.RecordingSize.Name = "RecordingSize";
            this.RecordingSize.Size = new System.Drawing.Size(100, 20);
            this.RecordingSize.TabIndex = 72;
            // 
            // editRecordingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(203, 196);
            this.ControlBox = false;
            this.Controls.Add(this.RecordingSize);
            this.Controls.Add(this.RecordingLength);
            this.Controls.Add(this.RecordingDate);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.CancelBookButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RecordingExt);
            this.Controls.Add(this.RecordingFileName);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.RecordingTitle);
            this.Name = "editRecordingForm";
            this.Text = "editRecordingForm";
            this.Load += new System.EventHandler(this.editRecordingForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RecordingDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecordingLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecordingSize)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CancelBookButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox RecordingExt;
        private System.Windows.Forms.TextBox RecordingFileName;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.TextBox RecordingTitle;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown RecordingDate;
        private System.Windows.Forms.NumericUpDown RecordingLength;
        private System.Windows.Forms.NumericUpDown RecordingSize;
    }
}