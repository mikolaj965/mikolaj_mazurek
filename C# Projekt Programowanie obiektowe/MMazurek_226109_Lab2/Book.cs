﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace MMazurek_226109_Lab2
{
    class Book : Position
    {
        private int pages;
        private bool cover;
        private string author;
        private editBookForm editFormH = null;
        private ListView bookList = null;

        public Book()
        {


        }

        public Book(int id, int uid, bool utype, string title, int date, int pages, bool cover, string author, string userLogin, string maxDate)
        {
            this.id = id;
            this.userID = uid;
            this.userType = utype;
            this.title = title;
            this.date = date;
            this.pages = pages;
            this.cover = cover;
            this.author = author;
            this.userLogin = userLogin;
            this.maxDate = maxDate;
        }

        public override void editPosition(Position obj, ListView list)
        {
            bookList = list;
            editFormH = new editBookForm(this, obj);
            var book = (Book)obj;

            editFormH.TitleTextBox = book.title;
            editFormH.AuthorTextBox = book.author;
            editFormH.DateTextBox = book.date;
            editFormH.PagesTextBox = book.pages;
            if(book.cover == true)
            {
                editFormH.CoverRadio1 = true;
            }
            else
            {
                editFormH.CoverRadio2 = true;
            }

            editFormH.ShowDialog();
        }

        public void SavePosition()
        {
            this.title = editFormH.TitleTextBox;
            this.author = editFormH.AuthorTextBox;
            this.date = editFormH.DateTextBox;
            this.pages = editFormH.PagesTextBox;
            this.cover = editFormH.CoverRadio1;

            ListViewItem item = bookList.FindItemWithText(id.ToString());
            item.SubItems[3].Text = editFormH.TitleTextBox;
            item.SubItems[4].Text = editFormH.DateTextBox.ToString();
            item.SubItems[5].Text = editFormH.PagesTextBox.ToString();
            item.SubItems[6].Text = (cover ? "MIĘKKA" : "TWARDA");
            item.SubItems[7].Text = editFormH.AuthorTextBox;
        }

        public override void generateXML(XmlTextWriter writer)
        {
            writer.WriteStartElement("book");
                writer.WriteStartElement("id");
                    writer.WriteString(id.ToString());
                writer.WriteEndElement();
            
                writer.WriteStartElement("uid");
                    writer.WriteString(userID.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("title");
                    writer.WriteString(title);
                writer.WriteEndElement();

                writer.WriteStartElement("date");
                    writer.WriteString(date.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("pages");
                    writer.WriteString(pages.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("cover");
                    writer.WriteString(Convert.ToInt16(cover).ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("author");
                    writer.WriteString(author);
                writer.WriteEndElement();

                writer.WriteStartElement("userLogin");
                    writer.WriteString(userLogin);
                writer.WriteEndElement();
            
                writer.WriteStartElement("maxDate");
                    writer.WriteString(maxDate);
                writer.WriteEndElement();
            writer.WriteEndElement();
        }
        
        public override void showPosition(ListView obj)
        {
            ListViewItem item = new ListViewItem(id.ToString());
            item.SubItems.Add(userID.ToString());
            item.SubItems.Add("Książka");
            item.SubItems.Add(title);
            item.SubItems.Add(date.ToString());
            item.SubItems.Add(pages.ToString());
            item.SubItems.Add(((cover == false) ? "TWARDA" : "MIĘKKA"));
            item.SubItems.Add(author);
            item.SubItems.Add("-");
            item.SubItems.Add("-");
            item.SubItems.Add("-");
            item.SubItems.Add("-");
            item.SubItems.Add(userLogin);
            item.SubItems.Add(maxDate);
            obj.Items.Add(item);
        }
    }
}
