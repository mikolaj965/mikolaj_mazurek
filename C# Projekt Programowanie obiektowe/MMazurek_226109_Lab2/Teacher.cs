﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace MMazurek_226109_Lab2
{
    class Teacher : User
    {
        private string startWork, title;

        public Teacher(int id, bool type, string name, string surname, int date, bool gender, int books, string startWork, string title)
        {
            this.id = id;
            this.type = type;
            this.name = name;
            this.surname = surname;
            this.date = date;
            this.gender = gender;
            this.startWork = startWork;
            this.title = title;
            this.books = books;
        }

        public override void showPosition(ListView obj)
        {
            ListViewItem item = new ListViewItem(id.ToString());
            item.SubItems.Add("Nauczyciel");
            item.SubItems.Add(title);
            item.SubItems.Add(name);
            item.SubItems.Add(surname);
            item.SubItems.Add(date.ToString());
            item.SubItems.Add(((gender == false) ? "M" : "K"));
            item.SubItems.Add("-");
            item.SubItems.Add(startWork);
            obj.Items.Add(item);
        }

        public override void generateXML(XmlTextWriter writer)
        {
            writer.WriteStartElement("teacher");
                writer.WriteStartElement("id");
                    writer.WriteString(id.ToString());
                writer.WriteEndElement();
            
                writer.WriteStartElement("name");
                    writer.WriteString(name);
                writer.WriteEndElement();

                writer.WriteStartElement("surname");
                    writer.WriteString(surname);
                writer.WriteEndElement();

                writer.WriteStartElement("date");
                    writer.WriteString(date.ToString());
                writer.WriteEndElement();
            
                writer.WriteStartElement("gender");
                    writer.WriteString(Convert.ToInt16(gender).ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("books");
                    writer.WriteString(books.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("title");
                    writer.WriteString(title);
                writer.WriteEndElement();
            
                writer.WriteStartElement("startWork");
                    writer.WriteString(startWork);
                writer.WriteEndElement();
            writer.WriteEndElement();
        }
    }
}
