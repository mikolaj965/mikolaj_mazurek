﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMazurek_226109_Lab2
{
    public partial class GivePosForm : Form
    {
        public GivePosForm()
        {
            InitializeComponent();
        }

        public bool Save = false;

        private void button1_Click(object sender, EventArgs e)
        {
            Save = true;
            this.Close();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GivePosForm_Load(object sender, EventArgs e)
        {

            DatePos.MinDate = DateTime.Now;
        }
    }
}
