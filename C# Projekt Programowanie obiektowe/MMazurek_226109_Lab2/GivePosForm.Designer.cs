﻿namespace MMazurek_226109_Lab2
{
    partial class GivePosForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Cancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.GivePos = new System.Windows.Forms.Button();
            this.DatePos = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(12, 51);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(75, 23);
            this.Cancel.TabIndex = 7;
            this.Cancel.Text = "Anuluj";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Wypożycz do:";
            // 
            // GivePos
            // 
            this.GivePos.Location = new System.Drawing.Point(137, 51);
            this.GivePos.Name = "GivePos";
            this.GivePos.Size = new System.Drawing.Size(75, 23);
            this.GivePos.TabIndex = 5;
            this.GivePos.Text = "Wypożycz";
            this.GivePos.UseVisualStyleBackColor = true;
            this.GivePos.Click += new System.EventHandler(this.button1_Click);
            // 
            // DatePos
            // 
            this.DatePos.Location = new System.Drawing.Point(12, 25);
            this.DatePos.MinDate = new System.DateTime(2016, 1, 1, 0, 0, 0, 0);
            this.DatePos.Name = "DatePos";
            this.DatePos.Size = new System.Drawing.Size(200, 20);
            this.DatePos.TabIndex = 4;
            // 
            // GivePosForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(218, 81);
            this.ControlBox = false;
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.GivePos);
            this.Controls.Add(this.DatePos);
            this.Name = "GivePosForm";
            this.Text = "Wypożyczanie";
            this.Load += new System.EventHandler(this.GivePosForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button GivePos;
        public System.Windows.Forms.DateTimePicker DatePos;
    }
}