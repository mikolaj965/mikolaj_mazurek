﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace MMazurek_226109_Lab2
{
    public partial class Form1 : Form
    { 
        private int clickedID;
        private Library lib;

        public Form1()
        {
            InitializeComponent();
            lib = new Library();
            lib.showPosition(positionView);
            lib.showUser(userView);
        }

        private void edycjaPozycjiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lib.editPosition(clickedID, positionView);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void positionView_MouseClick(object sender, MouseEventArgs e)
        {
            clickedID = Convert.ToInt32(positionView.SelectedItems[0].Text);
            if (e.Button == MouseButtons.Right)
            {
                if (positionView.FocusedItem.Bounds.Contains(e.Location) == true)
                {
                    contextMenuStrip1.Show(Cursor.Position);
                }
            }
        }

        private void usuńPozycjęToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            lib.deletePosition(clickedID, positionView);
        }

        private void positionView_DoubleClick(object sender, EventArgs e)
        {
            lib.editPosition(clickedID, positionView);

        }

        private void pozycjęToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lib.AddPosition(positionView);
        }

        private void userView_MouseClick(object sender, MouseEventArgs e)
        {
            clickedID = Convert.ToInt32(userView.SelectedItems[0].Text);
            if (e.Button == MouseButtons.Right)
            {
                if (userView.FocusedItem.Bounds.Contains(e.Location) == true)
                {
                    contextMenuStrip2.Show(Cursor.Position);
                }
            }
        }

        private void DeleteUserStrip_Click(object sender, EventArgs e)
        {
            lib.deleteUser(clickedID, userView);
        }

        private void użytkownikaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lib.AddUser(userView);
        }

        private void wypożyczToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (positionView.SelectedItems.Count == 0 || userView.SelectedItems.Count == 0)
            {
                MessageBox.Show("Aby wypożyczyć pozycję musisz zaznaczyć ksiażkę oraz użytkownika, któremu chcesz wypożyczyć", "Błąd");
                return;
            }
            int posID = Convert.ToInt32(positionView.SelectedItems[0].Text);
            int userID = Convert.ToInt32(userView.SelectedItems[0].Text);

            if (!lib.CheckAvailable(posID))
            {
                MessageBox.Show("Ta książka jest już wypożyczona, spróbuj inną", "Błąd");
                return;
            }

            if (!lib.CheckUserPos(userID))
            {
                MessageBox.Show("Podany użytkownik wypożyczył już trzy książki, nie możesz wypożyczyć dla niego", "Błąd");
                return;
            }

            if (lib.GetUserType(userID)) //nauczyciel bezterminowo wypozycza
            {
                string user = lib.GivePos(userID, posID, "bezterminowo");

                ListViewItem item = positionView.FindItemWithText(posID.ToString());
                item.SubItems[1].Text = userID.ToString();
                item.SubItems[12].Text = user;
                item.SubItems[13].Text = "bezterminowo";
            }
            else
            {
                GivePosForm givePos = new GivePosForm();
                givePos.Show();
                givePos.FormClosed += delegate (object s, FormClosedEventArgs a)
                {
                    if (givePos.Save)
                    {
                        string user = lib.GivePos(userID, posID, givePos.DatePos.Text);

                        ListViewItem item = positionView.FindItemWithText(posID.ToString());
                        item.SubItems[1].Text = userID.ToString();
                        item.SubItems[12].Text = user;
                        item.SubItems[13].Text = givePos.DatePos.Text;
                    }
                };
            }
        }

        private void oProgramieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Autor: Mikołaj Mazurek\nNr indeksu: 226109\nWszelkie prawa zastrzeżone","O programie...");
        }
    }
}
