﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace MMazurek_226109_Lab2
{
    class Recording : Position
    {
        private string ext, filename;
        private float length, size;
        private editRecordingForm editFormH = null;
        private ListView recordingList = null;

        public Recording(int id, int uid, bool utype, string title, int date, string ext, string filename, float length, float size, string userLogin, string maxDate)
        {
            this.id = id;
            this.userID = uid;
            this.userType = utype;
            this.title = title;
            this.date = date;
            this.ext = ext;
            this.filename = filename;
            this.length = length;
            this.size = size;
            this.userLogin = userLogin;
            this.maxDate = maxDate;
        }

        public override void editPosition(Position obj, ListView list)
        {
            recordingList = list;
            editFormH = new editRecordingForm(this, obj);
            var recording = (Recording)obj;

            editFormH.TitleTextBox = recording.title;
            editFormH.DateTextBox = recording.date;
            editFormH.FilenameTextBox= recording.filename;
            editFormH.ExtTextBox = recording.ext;
            editFormH.SizeTextBox = recording.size;
            editFormH.LengthTextBox = recording.length;

            editFormH.ShowDialog();
        }

        public void SavePosition()
        {
            this.title = editFormH.TitleTextBox;
            this.date = editFormH.DateTextBox;
            this.ext = editFormH.ExtTextBox;
            this.filename = editFormH.FilenameTextBox;
            this.size = editFormH.SizeTextBox;
            this.length = editFormH.LengthTextBox;

            ListViewItem item = recordingList.FindItemWithText(id.ToString());
            item.SubItems[3].Text = editFormH.TitleTextBox;
            item.SubItems[4].Text = editFormH.DateTextBox.ToString();
            item.SubItems[8].Text = editFormH.ExtTextBox;
            item.SubItems[9].Text = editFormH.FilenameTextBox;
            item.SubItems[10].Text = editFormH.LengthTextBox.ToString();
            item.SubItems[11].Text = editFormH.SizeTextBox.ToString();
        }

        public override void generateXML(XmlTextWriter writer)
        {
            writer.WriteStartElement("recording");
                writer.WriteStartElement("id");
                    writer.WriteString(id.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("uid");
                    writer.WriteString(userID.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("title");
                    writer.WriteString(title);
                writer.WriteEndElement();

                writer.WriteStartElement("date");
                    writer.WriteString(date.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("ext");
                    writer.WriteString(ext);
                writer.WriteEndElement();

                writer.WriteStartElement("filename");
                    writer.WriteString(filename);
                writer.WriteEndElement();

                writer.WriteStartElement("length");
                    writer.WriteString(length.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("size");
                    writer.WriteString(size.ToString());
                writer.WriteEndElement();
            
                writer.WriteStartElement("userLogin");
                    writer.WriteString(userLogin);
                writer.WriteEndElement();
            
                writer.WriteStartElement("maxDate");
                    writer.WriteString(maxDate);
                writer.WriteEndElement();

            writer.WriteEndElement();
        }

        public override void showPosition(ListView obj)
        {
            ListViewItem item = new ListViewItem(id.ToString());
            item.SubItems.Add(userID.ToString());
            item.SubItems.Add("Nagranie");
            item.SubItems.Add(title);
            item.SubItems.Add(date.ToString());
            item.SubItems.Add("-");
            item.SubItems.Add("-");
            item.SubItems.Add("-");
            item.SubItems.Add(ext);
            item.SubItems.Add(filename);
            item.SubItems.Add(length.ToString());
            item.SubItems.Add(size.ToString());
            item.SubItems.Add(userLogin);
            item.SubItems.Add(maxDate);
            obj.Items.Add(item);
        }
    }
}
