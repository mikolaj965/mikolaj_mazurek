﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace MMazurek_226109_Lab2
{
    class Newspaper : Position
    {
        private int pages;
        private editNewspaperForm editFormH = null;
        private ListView newspaperList = null;

        public Newspaper(int id, int uid, bool utype, string title, int date, int pages, string userLogin, string maxDate)
        {
            this.id = id;
            this.userID = uid;
            this.userType = utype;
            this.title = title;
            this.date = date;
            this.pages = pages;
            this.userLogin = userLogin;
            this.maxDate = maxDate;
        }

        public void SavePosition()
        {
            this.title = editFormH.TitleTextBox;
            this.date = editFormH.DateTextBox;
            this.pages = editFormH.PagesTextBox;

            ListViewItem item = newspaperList.FindItemWithText(id.ToString());
            item.SubItems[3].Text = editFormH.TitleTextBox;
            item.SubItems[4].Text = editFormH.DateTextBox.ToString();
            item.SubItems[5].Text = editFormH.PagesTextBox.ToString();
        }

        public override void generateXML(XmlTextWriter writer)
        {
            writer.WriteStartElement("newspaper");
                writer.WriteStartElement("id");
                    writer.WriteString(id.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("uid");
                    writer.WriteString(userID.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("title");
                    writer.WriteString(title);
                writer.WriteEndElement();

                writer.WriteStartElement("date");
                    writer.WriteString(date.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("pages");
                    writer.WriteString(pages.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("userLogin");
                    writer.WriteString(userLogin);
                writer.WriteEndElement();
            
                writer.WriteStartElement("maxDate");
                    writer.WriteString(maxDate);
                writer.WriteEndElement();
            writer.WriteEndElement();
        }

        public override void editPosition(Position obj, ListView list)
        {
            newspaperList = list;
            editFormH = new editNewspaperForm(this, obj);
            var book = (Newspaper)obj;

            editFormH.TitleTextBox = book.title;
            editFormH.DateTextBox = book.date;
            editFormH.PagesTextBox = book.pages;
            editFormH.ShowDialog();
        }

        public override void showPosition(ListView obj)
        {
            ListViewItem item = new ListViewItem(id.ToString());
            item.SubItems.Add(userID.ToString());
            item.SubItems.Add("Gazeta");
            item.SubItems.Add(title);
            item.SubItems.Add(date.ToString());
            item.SubItems.Add(pages.ToString());
            item.SubItems.Add("-");
            item.SubItems.Add("-");
            item.SubItems.Add("-");
            item.SubItems.Add("-");
            item.SubItems.Add("-");
            item.SubItems.Add("-");
            item.SubItems.Add(userLogin);
            item.SubItems.Add(maxDate);
            obj.Items.Add(item);
        }
    }
}
