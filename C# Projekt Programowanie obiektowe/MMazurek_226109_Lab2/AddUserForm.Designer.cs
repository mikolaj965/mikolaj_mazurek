﻿namespace MMazurek_226109_Lab2
{
    partial class AddUserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.AddTitle = new System.Windows.Forms.TextBox();
            this.UserType = new System.Windows.Forms.GroupBox();
            this.UserType2 = new System.Windows.Forms.RadioButton();
            this.UserType1 = new System.Windows.Forms.RadioButton();
            this.CancelBookButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.AddStartWork = new System.Windows.Forms.TextBox();
            this.AddSurname = new System.Windows.Forms.TextBox();
            this.Add = new System.Windows.Forms.Button();
            this.AddName = new System.Windows.Forms.TextBox();
            this.AddDate = new System.Windows.Forms.NumericUpDown();
            this.GenderType = new System.Windows.Forms.GroupBox();
            this.GenderType2 = new System.Windows.Forms.RadioButton();
            this.GenderType1 = new System.Windows.Forms.RadioButton();
            this.AddAlbum = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.UserType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AddDate)).BeginInit();
            this.GenderType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AddAlbum)).BeginInit();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 179);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 66;
            this.label8.Text = "Rok urodzenia:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 156);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 13);
            this.label7.TabIndex = 64;
            this.label7.Text = "Tytuł naukowy:";
            // 
            // AddTitle
            // 
            this.AddTitle.Enabled = false;
            this.AddTitle.Location = new System.Drawing.Point(112, 153);
            this.AddTitle.Name = "AddTitle";
            this.AddTitle.Size = new System.Drawing.Size(100, 20);
            this.AddTitle.TabIndex = 63;
            // 
            // UserType
            // 
            this.UserType.Controls.Add(this.UserType2);
            this.UserType.Controls.Add(this.UserType1);
            this.UserType.Location = new System.Drawing.Point(12, 12);
            this.UserType.Name = "UserType";
            this.UserType.Size = new System.Drawing.Size(200, 42);
            this.UserType.TabIndex = 62;
            this.UserType.TabStop = false;
            this.UserType.Text = "Typ użytkownika";
            // 
            // UserType2
            // 
            this.UserType2.AutoSize = true;
            this.UserType2.Location = new System.Drawing.Point(100, 19);
            this.UserType2.Name = "UserType2";
            this.UserType2.Size = new System.Drawing.Size(77, 17);
            this.UserType2.TabIndex = 5;
            this.UserType2.TabStop = true;
            this.UserType2.Text = "Nauczyciel";
            this.UserType2.UseVisualStyleBackColor = true;
            this.UserType2.CheckedChanged += new System.EventHandler(this.UserType2_CheckedChanged);
            // 
            // UserType1
            // 
            this.UserType1.AutoSize = true;
            this.UserType1.Checked = true;
            this.UserType1.Location = new System.Drawing.Point(12, 19);
            this.UserType1.Name = "UserType1";
            this.UserType1.Size = new System.Drawing.Size(62, 17);
            this.UserType1.TabIndex = 6;
            this.UserType1.TabStop = true;
            this.UserType1.Text = "Student";
            this.UserType1.UseVisualStyleBackColor = true;
            this.UserType1.CheckedChanged += new System.EventHandler(this.UserType1_CheckedChanged);
            // 
            // CancelBookButton
            // 
            this.CancelBookButton.Location = new System.Drawing.Point(19, 267);
            this.CancelBookButton.Name = "CancelBookButton";
            this.CancelBookButton.Size = new System.Drawing.Size(75, 23);
            this.CancelBookButton.TabIndex = 57;
            this.CancelBookButton.Text = "Anuluj";
            this.CancelBookButton.UseVisualStyleBackColor = true;
            this.CancelBookButton.Click += new System.EventHandler(this.CancelBookButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 229);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 26);
            this.label4.TabIndex = 56;
            this.label4.Text = "Data rozp. pracy\r\n(DD-MM-YYYY)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 54;
            this.label2.Text = "Nazwisko";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 105);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 53;
            this.label1.Text = "Imię";
            // 
            // AddStartWork
            // 
            this.AddStartWork.Enabled = false;
            this.AddStartWork.Location = new System.Drawing.Point(112, 226);
            this.AddStartWork.Name = "AddStartWork";
            this.AddStartWork.Size = new System.Drawing.Size(100, 20);
            this.AddStartWork.TabIndex = 52;
            // 
            // AddSurname
            // 
            this.AddSurname.Location = new System.Drawing.Point(112, 127);
            this.AddSurname.Name = "AddSurname";
            this.AddSurname.Size = new System.Drawing.Size(100, 20);
            this.AddSurname.TabIndex = 50;
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(125, 267);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(75, 23);
            this.Add.TabIndex = 49;
            this.Add.Text = "Dodaj";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // AddName
            // 
            this.AddName.Location = new System.Drawing.Point(112, 102);
            this.AddName.Name = "AddName";
            this.AddName.Size = new System.Drawing.Size(100, 20);
            this.AddName.TabIndex = 48;
            // 
            // AddDate
            // 
            this.AddDate.Location = new System.Drawing.Point(112, 177);
            this.AddDate.Maximum = new decimal(new int[] {
            2016,
            0,
            0,
            0});
            this.AddDate.Minimum = new decimal(new int[] {
            1900,
            0,
            0,
            0});
            this.AddDate.Name = "AddDate";
            this.AddDate.Size = new System.Drawing.Size(100, 20);
            this.AddDate.TabIndex = 67;
            this.AddDate.Value = new decimal(new int[] {
            1900,
            0,
            0,
            0});
            // 
            // GenderType
            // 
            this.GenderType.Controls.Add(this.GenderType2);
            this.GenderType.Controls.Add(this.GenderType1);
            this.GenderType.Location = new System.Drawing.Point(12, 54);
            this.GenderType.Name = "GenderType";
            this.GenderType.Size = new System.Drawing.Size(200, 42);
            this.GenderType.TabIndex = 68;
            this.GenderType.TabStop = false;
            this.GenderType.Text = "Płeć";
            // 
            // GenderType2
            // 
            this.GenderType2.AutoSize = true;
            this.GenderType2.Location = new System.Drawing.Point(100, 19);
            this.GenderType2.Name = "GenderType2";
            this.GenderType2.Size = new System.Drawing.Size(61, 17);
            this.GenderType2.TabIndex = 5;
            this.GenderType2.TabStop = true;
            this.GenderType2.Text = "Kobieta";
            this.GenderType2.UseVisualStyleBackColor = true;
            // 
            // GenderType1
            // 
            this.GenderType1.AutoSize = true;
            this.GenderType1.Checked = true;
            this.GenderType1.Location = new System.Drawing.Point(12, 19);
            this.GenderType1.Name = "GenderType1";
            this.GenderType1.Size = new System.Drawing.Size(78, 17);
            this.GenderType1.TabIndex = 6;
            this.GenderType1.TabStop = true;
            this.GenderType1.Text = "Mężczyzna";
            this.GenderType1.UseVisualStyleBackColor = true;
            // 
            // AddAlbum
            // 
            this.AddAlbum.Location = new System.Drawing.Point(112, 203);
            this.AddAlbum.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.AddAlbum.Name = "AddAlbum";
            this.AddAlbum.Size = new System.Drawing.Size(100, 20);
            this.AddAlbum.TabIndex = 70;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 205);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 69;
            this.label9.Text = "Nr albumu";
            // 
            // AddUserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(222, 298);
            this.ControlBox = false;
            this.Controls.Add(this.AddAlbum);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.GenderType);
            this.Controls.Add(this.AddDate);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.AddTitle);
            this.Controls.Add(this.UserType);
            this.Controls.Add(this.CancelBookButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AddStartWork);
            this.Controls.Add(this.AddSurname);
            this.Controls.Add(this.Add);
            this.Controls.Add(this.AddName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddUserForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Dodawanie Użytkownika";
            this.UserType.ResumeLayout(false);
            this.UserType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AddDate)).EndInit();
            this.GenderType.ResumeLayout(false);
            this.GenderType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AddAlbum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox AddTitle;
        private System.Windows.Forms.GroupBox UserType;
        private System.Windows.Forms.RadioButton UserType2;
        private System.Windows.Forms.RadioButton UserType1;
        private System.Windows.Forms.Button CancelBookButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox AddStartWork;
        private System.Windows.Forms.TextBox AddSurname;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.TextBox AddName;
        private System.Windows.Forms.NumericUpDown AddDate;
        private System.Windows.Forms.GroupBox GenderType;
        private System.Windows.Forms.RadioButton GenderType2;
        private System.Windows.Forms.RadioButton GenderType1;
        private System.Windows.Forms.NumericUpDown AddAlbum;
        private System.Windows.Forms.Label label9;
    }
}