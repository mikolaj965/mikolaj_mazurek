﻿namespace MMazurek_226109_Lab2
{
    partial class editNewspaperForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CancelNewspaperButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Save = new System.Windows.Forms.Button();
            this.NewspaperTitle = new System.Windows.Forms.TextBox();
            this.NewspaperPages = new System.Windows.Forms.NumericUpDown();
            this.NewspaperDate = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.NewspaperPages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewspaperDate)).BeginInit();
            this.SuspendLayout();
            // 
            // CancelNewspaperButton
            // 
            this.CancelNewspaperButton.Location = new System.Drawing.Point(116, 101);
            this.CancelNewspaperButton.Name = "CancelNewspaperButton";
            this.CancelNewspaperButton.Size = new System.Drawing.Size(75, 23);
            this.CancelNewspaperButton.TabIndex = 23;
            this.CancelNewspaperButton.Text = "Anuluj";
            this.CancelNewspaperButton.UseVisualStyleBackColor = true;
            this.CancelNewspaperButton.Click += new System.EventHandler(this.CancelNewspaperButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Ilość stron:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Data:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Tytuł:";
            // 
            // Save
            // 
            this.Save.Location = new System.Drawing.Point(197, 101);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(75, 23);
            this.Save.TabIndex = 14;
            this.Save.Text = "Zapisz";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // NewspaperTitle
            // 
            this.NewspaperTitle.Location = new System.Drawing.Point(73, 12);
            this.NewspaperTitle.Name = "NewspaperTitle";
            this.NewspaperTitle.Size = new System.Drawing.Size(120, 20);
            this.NewspaperTitle.TabIndex = 13;
            // 
            // NewspaperPages
            // 
            this.NewspaperPages.Location = new System.Drawing.Point(73, 65);
            this.NewspaperPages.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.NewspaperPages.Name = "NewspaperPages";
            this.NewspaperPages.Size = new System.Drawing.Size(120, 20);
            this.NewspaperPages.TabIndex = 25;
            // 
            // NewspaperDate
            // 
            this.NewspaperDate.Location = new System.Drawing.Point(73, 39);
            this.NewspaperDate.Maximum = new decimal(new int[] {
            2016,
            0,
            0,
            0});
            this.NewspaperDate.Name = "NewspaperDate";
            this.NewspaperDate.Size = new System.Drawing.Size(120, 20);
            this.NewspaperDate.TabIndex = 24;
            // 
            // editNewspaperForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 132);
            this.ControlBox = false;
            this.Controls.Add(this.NewspaperPages);
            this.Controls.Add(this.NewspaperDate);
            this.Controls.Add(this.CancelNewspaperButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.NewspaperTitle);
            this.Name = "editNewspaperForm";
            this.Text = "Edycja gazety";
            ((System.ComponentModel.ISupportInitialize)(this.NewspaperPages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewspaperDate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CancelNewspaperButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.TextBox NewspaperTitle;
        private System.Windows.Forms.NumericUpDown NewspaperPages;
        private System.Windows.Forms.NumericUpDown NewspaperDate;
    }
}