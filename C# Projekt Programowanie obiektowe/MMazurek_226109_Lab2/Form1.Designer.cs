﻿namespace MMazurek_226109_Lab2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dodajToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pozycjęToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.użytkownikaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wypożyczToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oProgramieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.edycjaPozycjiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuńPozycjęToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.positionView = new System.Windows.Forms.ListView();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader27 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader28 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.userView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.DeleteUserStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.menuStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajToolStripMenuItem1,
            this.wypożyczToolStripMenuItem,
            this.oProgramieToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1436, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dodajToolStripMenuItem1
            // 
            this.dodajToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pozycjęToolStripMenuItem,
            this.użytkownikaToolStripMenuItem});
            this.dodajToolStripMenuItem1.Name = "dodajToolStripMenuItem1";
            this.dodajToolStripMenuItem1.Size = new System.Drawing.Size(59, 20);
            this.dodajToolStripMenuItem1.Text = "Dodaj...";
            // 
            // pozycjęToolStripMenuItem
            // 
            this.pozycjęToolStripMenuItem.Name = "pozycjęToolStripMenuItem";
            this.pozycjęToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.pozycjęToolStripMenuItem.Text = "Pozycję";
            this.pozycjęToolStripMenuItem.Click += new System.EventHandler(this.pozycjęToolStripMenuItem_Click);
            // 
            // użytkownikaToolStripMenuItem
            // 
            this.użytkownikaToolStripMenuItem.Name = "użytkownikaToolStripMenuItem";
            this.użytkownikaToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.użytkownikaToolStripMenuItem.Text = "Użytkownika";
            this.użytkownikaToolStripMenuItem.Click += new System.EventHandler(this.użytkownikaToolStripMenuItem_Click);
            // 
            // wypożyczToolStripMenuItem
            // 
            this.wypożyczToolStripMenuItem.Name = "wypożyczToolStripMenuItem";
            this.wypożyczToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.wypożyczToolStripMenuItem.Text = "Wypożycz";
            this.wypożyczToolStripMenuItem.Click += new System.EventHandler(this.wypożyczToolStripMenuItem_Click);
            // 
            // oProgramieToolStripMenuItem
            // 
            this.oProgramieToolStripMenuItem.Name = "oProgramieToolStripMenuItem";
            this.oProgramieToolStripMenuItem.Size = new System.Drawing.Size(86, 20);
            this.oProgramieToolStripMenuItem.Text = "O programie";
            this.oProgramieToolStripMenuItem.Click += new System.EventHandler(this.oProgramieToolStripMenuItem_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.edycjaPozycjiToolStripMenuItem,
            this.usuńPozycjęToolStripMenuItem1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(158, 48);
            // 
            // edycjaPozycjiToolStripMenuItem
            // 
            this.edycjaPozycjiToolStripMenuItem.Name = "edycjaPozycjiToolStripMenuItem";
            this.edycjaPozycjiToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.edycjaPozycjiToolStripMenuItem.Text = "Edycja pozycji...";
            this.edycjaPozycjiToolStripMenuItem.Click += new System.EventHandler(this.edycjaPozycjiToolStripMenuItem_Click);
            // 
            // usuńPozycjęToolStripMenuItem1
            // 
            this.usuńPozycjęToolStripMenuItem1.Name = "usuńPozycjęToolStripMenuItem1";
            this.usuńPozycjęToolStripMenuItem1.Size = new System.Drawing.Size(157, 22);
            this.usuńPozycjęToolStripMenuItem1.Text = "Usuń pozycję...";
            this.usuńPozycjęToolStripMenuItem1.Click += new System.EventHandler(this.usuńPozycjęToolStripMenuItem1_Click);
            // 
            // positionView
            // 
            this.positionView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader17,
            this.columnHeader23,
            this.columnHeader18,
            this.columnHeader19,
            this.columnHeader20,
            this.columnHeader21,
            this.columnHeader22,
            this.columnHeader24,
            this.columnHeader25,
            this.columnHeader26,
            this.columnHeader27,
            this.columnHeader28,
            this.columnHeader2});
            this.positionView.FullRowSelect = true;
            this.positionView.GridLines = true;
            this.positionView.HideSelection = false;
            this.positionView.Location = new System.Drawing.Point(12, 49);
            this.positionView.MultiSelect = false;
            this.positionView.Name = "positionView";
            this.positionView.Size = new System.Drawing.Size(909, 404);
            this.positionView.TabIndex = 2;
            this.positionView.UseCompatibleStateImageBehavior = false;
            this.positionView.View = System.Windows.Forms.View.Details;
            this.positionView.DoubleClick += new System.EventHandler(this.positionView_DoubleClick);
            this.positionView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.positionView_MouseClick);
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "ID";
            this.columnHeader7.Width = 24;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "UID";
            this.columnHeader17.Width = 32;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "Rodzaj";
            this.columnHeader23.Width = 45;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "Tytuł pozycji";
            this.columnHeader18.Width = 150;
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Data";
            this.columnHeader19.Width = 40;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Ilość stron";
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "Okładka";
            this.columnHeader21.Width = 55;
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "Autor";
            this.columnHeader22.Width = 77;
            // 
            // columnHeader24
            // 
            this.columnHeader24.Text = "Rozszerzenie";
            this.columnHeader24.Width = 77;
            // 
            // columnHeader25
            // 
            this.columnHeader25.Text = "Nazwa pliku";
            this.columnHeader25.Width = 100;
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "Dlugość";
            this.columnHeader26.Width = 55;
            // 
            // columnHeader27
            // 
            this.columnHeader27.Text = "Rozmiar";
            this.columnHeader27.Width = 50;
            // 
            // columnHeader28
            // 
            this.columnHeader28.Text = "Wypożyczył";
            this.columnHeader28.Width = 70;
            // 
            // userView
            // 
            this.userView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader3,
            this.columnHeader12,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11});
            this.userView.FullRowSelect = true;
            this.userView.GridLines = true;
            this.userView.HideSelection = false;
            this.userView.Location = new System.Drawing.Point(927, 49);
            this.userView.MultiSelect = false;
            this.userView.Name = "userView";
            this.userView.Size = new System.Drawing.Size(497, 404);
            this.userView.TabIndex = 3;
            this.userView.UseCompatibleStateImageBehavior = false;
            this.userView.View = System.Windows.Forms.View.Details;
            this.userView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.userView_MouseClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "UID";
            this.columnHeader1.Width = 32;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Rodzaj";
            this.columnHeader3.Width = 50;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Tytuł nauk.";
            this.columnHeader12.Width = 55;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Imię";
            this.columnHeader4.Width = 70;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Nazwisko";
            this.columnHeader5.Width = 70;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Data";
            this.columnHeader8.Width = 45;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Płeć";
            this.columnHeader9.Width = 35;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Album";
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Pracuje od";
            this.columnHeader11.Width = 75;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(197, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Dostępne pozycje w systemie:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(924, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(219, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Dostępni użytkownicy w systemie:";
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DeleteUserStrip});
            this.contextMenuStrip2.Name = "contextMenuStrip1";
            this.contextMenuStrip2.Size = new System.Drawing.Size(180, 26);
            // 
            // DeleteUserStrip
            // 
            this.DeleteUserStrip.Name = "DeleteUserStrip";
            this.DeleteUserStrip.Size = new System.Drawing.Size(179, 22);
            this.DeleteUserStrip.Text = "Usuń użytkownika...";
            this.DeleteUserStrip.Click += new System.EventHandler(this.DeleteUserStrip_Click);
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Data zwrotu";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1436, 500);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.userView);
            this.Controls.Add(this.positionView);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Obsluga biblioteki";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem edycjaPozycjiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuńPozycjęToolStripMenuItem1;
        public System.Windows.Forms.ListView positionView;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.ColumnHeader columnHeader28;
        private System.Windows.Forms.ToolStripMenuItem dodajToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem wypożyczToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oProgramieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pozycjęToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem użytkownikaToolStripMenuItem;
        public System.Windows.Forms.ListView userView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem DeleteUserStrip;
        private System.Windows.Forms.ColumnHeader columnHeader2;
    }
}

