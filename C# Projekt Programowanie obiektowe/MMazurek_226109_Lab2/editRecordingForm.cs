﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMazurek_226109_Lab2
{
    public partial class editRecordingForm : Form
    {
        private Position position = null; 

        public editRecordingForm(object sender, Position obj)
        {
            position = obj;
            InitializeComponent();
        }

        public string TitleTextBox
        {
            get
            {
                return RecordingTitle.Text;
            }
            set
            {
                RecordingTitle.Text = value;
            }
        }

        public string ExtTextBox
        {
            get
            {
                return RecordingExt.Text;
            }
            set
            {
                RecordingExt.Text = value;
            }
        }

        public string FilenameTextBox
        {
            get
            {
                return RecordingFileName.Text;
            }
            set
            {
                RecordingFileName.Text = value;
            }
        }

        public int DateTextBox
        {
            get
            {
                return int.Parse(RecordingDate.Text);
            }
            set
            {
                RecordingDate.Text = value.ToString();
            }
        }

        public float LengthTextBox
        {
            get
            {
                return float.Parse(RecordingLength.Text);
            }
            set
            {
                RecordingLength.Text = value.ToString();
            }
        }

        public float SizeTextBox
        {
            get
            {
                return float.Parse(RecordingSize.Text);
            }
            set
            {
                RecordingSize.Text = value.ToString();
            }
        }



        private void Save_Click(object sender, EventArgs e)
        {
            Recording recording = (Recording)position;
            recording.SavePosition();
            this.Close();

        }

        private void editRecordingForm_Load(object sender, EventArgs e)
        {
            
        }

        private void CancelBookButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
