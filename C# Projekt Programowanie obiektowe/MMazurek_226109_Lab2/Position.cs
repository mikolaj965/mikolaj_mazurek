﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace MMazurek_226109_Lab2
{
    public class Position
    {

        protected int id, date, userID;
        protected bool userType;
        protected string title, userLogin, maxDate;

        public Position()
        {
            
        }

        public int getPositionID()
        {
            return id;
        }

        public virtual void editPosition(Position obj, ListView list)
        {

        }

        public Position AddPosition(ListView list)
        {
            AddPositionForm a = new AddPositionForm();
            a.ShowDialog();
            if (a.Save)
            {
                if (a.PositionRadio1)
                {
                    ListViewItem item = new ListViewItem(Library.getNextID().ToString());
                    item.SubItems.Add("0");
                    item.SubItems.Add("Książka");
                    item.SubItems.Add(a.TitleTextBox);
                    item.SubItems.Add(a.DateTextBox.ToString());
                    item.SubItems.Add(a.PagesTextBox.ToString());
                    item.SubItems.Add(((a.CoverRadio1 == false) ? "TWARDA" : "MIĘKKA"));
                    item.SubItems.Add(a.AuthorTextBox);
                    item.SubItems.Add("-");
                    item.SubItems.Add("-");
                    item.SubItems.Add("-");
                    item.SubItems.Add("-");
                    item.SubItems.Add("---");
                    item.SubItems.Add("---");
                    list.Items.Add(item);
                    Book obj = new Book(Library.getNextID(), 0, false, a.TitleTextBox, a.DateTextBox, a.PagesTextBox, a.CoverRadio1, a.AuthorTextBox, "---", "---");

                    Library.enlargeID();
                    return obj;
                }
                else if (a.PositionRadio2)
                {
                    ListViewItem item = new ListViewItem(Library.getNextID().ToString());
                    item.SubItems.Add("0");
                    item.SubItems.Add("Gazeta");
                    item.SubItems.Add(a.TitleTextBox);
                    item.SubItems.Add(a.DateTextBox.ToString());
                    item.SubItems.Add(a.PagesTextBox.ToString());
                    item.SubItems.Add("-");
                    item.SubItems.Add("-");
                    item.SubItems.Add("-");
                    item.SubItems.Add("-");
                    item.SubItems.Add("-");
                    item.SubItems.Add("-");
                    item.SubItems.Add("---");
                    item.SubItems.Add("---");
                    list.Items.Add(item);
                    Newspaper obj = new Newspaper(Library.getNextID(), 0, false, a.TitleTextBox, a.DateTextBox, a.PagesTextBox, "---", "---");

                    Library.enlargeID();
                    return obj;
                }
                else if (a.PositionRadio3)
                {
                    ListViewItem item = new ListViewItem(Library.getNextID().ToString());
                    item.SubItems.Add("0");
                    item.SubItems.Add("Nagranie");
                    item.SubItems.Add(a.TitleTextBox);
                    item.SubItems.Add(a.DateTextBox.ToString());
                    item.SubItems.Add("-");
                    item.SubItems.Add("-");
                    item.SubItems.Add("-");
                    item.SubItems.Add(a.ExtTextBox);
                    item.SubItems.Add(a.FilenameTextBox);
                    item.SubItems.Add(a.LengthTextBox.ToString());
                    item.SubItems.Add(a.SizeTextBox.ToString());
                    item.SubItems.Add("---");
                    item.SubItems.Add("---");
                    list.Items.Add(item);
                    Recording obj = new Recording(Library.getNextID(), 0, false, a.TitleTextBox, a.DateTextBox, a.ExtTextBox, a.FilenameTextBox, a.LengthTextBox, a.SizeTextBox, "---", "---");

                    Library.enlargeID();
                    return obj;
                }

            }

            return new Position();
        }
        


        public virtual void generateXML(XmlTextWriter writer)
        {

        }

        public virtual void showPosition(ListView obj)
        {

        }

        public int GetUserID()
        {
            return userID;
        }

        public void givePos(int uid, string userLogin, string maxDate)
        {
            this.userID = uid;
            this.userLogin = userLogin;
            this.maxDate = maxDate;
        }
    }
}
