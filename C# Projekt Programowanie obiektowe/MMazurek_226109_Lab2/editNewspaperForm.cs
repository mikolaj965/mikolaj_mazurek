﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMazurek_226109_Lab2
{
    public partial class editNewspaperForm : Form
    {
        private Position position = null;

        public editNewspaperForm(object sender, Position obj)
        {
            position = obj;
            InitializeComponent();
        }

        public string TitleTextBox
        {
            get
            {
                return NewspaperTitle.Text;
            }
            set
            {
                NewspaperTitle.Text = value;
            }
        }

        public int DateTextBox
        {
            get
            {
                return Convert.ToInt16(NewspaperDate.Text);
            }
            set
            {
                NewspaperDate.Text = value.ToString();
            }
        }

        public int PagesTextBox
        {
            get
            {
                return Convert.ToInt16(NewspaperPages.Text);
            }
            set
            {
                NewspaperPages.Text = value.ToString();
            }
        }

        private void Save_Click(object sender, EventArgs e)
        {
            Newspaper newspaper = (Newspaper)position;
            newspaper.SavePosition();
            this.Close();
        }

        private void CancelNewspaperButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
