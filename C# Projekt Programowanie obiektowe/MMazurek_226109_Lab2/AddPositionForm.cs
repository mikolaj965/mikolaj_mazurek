﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMazurek_226109_Lab2
{
    public partial class AddPositionForm : Form
    { 
        private int CoverVal = 0;
        private int TypeVal = 0;

        public AddPositionForm()
        {
            InitializeComponent();
        }

        public string TitleTextBox
        {
            get
            {
                return AddTitle.Text;
            }
        }

        public int DateTextBox
        {
            get
            {
                return Convert.ToInt32(AddDate.Text);
            }
        }

        public int PagesTextBox
        {
            get
            {
                return Convert.ToInt32(AddPages.Text);
            }
        }
        public float LengthTextBox
        {
            get
            {
                return float.Parse(AddLength.Text);
            }
        }
        public float SizeTextBox
        {
            get
            {
                return float.Parse(AddSize.Text);
            }
        }

        public bool CoverRadio2
        {
            get
            {
                return BookCover2.Checked;
            }
        }

        public bool CoverRadio1
        {
            get
            {
                return BookCover1.Checked;
            }
        }

        public string AuthorTextBox
        {
            get
            {
                return AddAuthor.Text;
            }
        }

        public string ExtTextBox
        {
            get
            {
                return AddExt.Text;
            }
        }
        
        public string FilenameTextBox
        {
            get
            {
                return AddFileName.Text;
            }
        }

        public bool PositionRadio1
        {
            get
            {
                return PositionType1.Checked;
            }
        }

        public bool PositionRadio2
        {
            get
            {
                return PositionType2.Checked;
            }
        }

        public bool PositionRadio3
        {
            get
            {
                return PositionType3.Checked;
            }
        }



        private void CancelBookButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PositionType1_CheckedChanged(object sender, EventArgs e)
        {
            this.TypeVal = 0;
            BookCover.Enabled = true;

            AddAuthor.Enabled = true;
            AddPages.Enabled = true;
            AddExt.Enabled = false;
            AddLength.Enabled = false;
            AddSize.Enabled = false;
            AddFileName.Enabled = false;
        }

        private void PositionType2_CheckedChanged(object sender, EventArgs e)
        {
            this.TypeVal = 1;
            BookCover.Enabled = false;

            AddAuthor.Enabled = false;
            AddPages.Enabled = true;
            AddExt.Enabled = false;
            AddLength.Enabled = false;
            AddSize.Enabled = false;
            AddFileName.Enabled = false;
        }

        private void PositionType3_CheckedChanged(object sender, EventArgs e)
        {
            this.TypeVal = 2;
            BookCover.Enabled = false;

            AddAuthor.Enabled = false;
            AddPages.Enabled = false;
            AddExt.Enabled = true;
            AddLength.Enabled = true;
            AddSize.Enabled = true;
            AddFileName.Enabled = true;

        }
        public bool Save = false;


        private void BookCover1_CheckedChanged(object sender, EventArgs e)
        {
            this.CoverVal = 0;
        }

        private void BookCover2_CheckedChanged(object sender, EventArgs e)
        {
            this.CoverVal = 0;
        }

        private void Add_Click(object sender, EventArgs e)
        {
            Save = true;
            this.Close();
        }
    }
}
