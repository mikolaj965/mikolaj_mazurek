﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMazurek_226109_Lab2
{
    public partial class AddUserForm : Form
    {
        public AddUserForm()
        {
            InitializeComponent();
        }

        public string NameTextBox
        {
            get
            {
                return AddName.Text;
            }
        }

        public string SurnameTextBox
        {
            get
            {
                return AddSurname.Text;
            }
        }

        public string TitleTextBox
        {
            get
            {
                return AddTitle.Text;
            }
        }

        public int DateTextBox
        {
            get
            {
                return int.Parse(AddDate.Text);
            }
        }

        public int AlbumTextBox
        {
            get
            {
                return Convert.ToInt32(AddAlbum.Text);
            }
        }

        public bool UserTypeRadio1
        {
            get
            {
                return UserType1.Checked;
            }
        }

        public bool UserTypeRadio2
        {
            get
            {
                return UserType2.Checked;
            }
        }

        public bool GenderTypeRadio1
        {
            get
            {
                return GenderType1.Checked;
            }
        }

        public bool GenderTypeRadio2
        {
            get
            {
                return GenderType2.Checked;
            }
        }

        public string StartWorkTextBox
        {
            get
            {
                return AddStartWork.Text;
            }
        }

        public bool Save = false;

        private void UserType1_CheckedChanged(object sender, EventArgs e)
        {
            AddTitle.Enabled = false;
            AddAlbum.Enabled = true;
            AddStartWork.Enabled = false;
        }

        private void UserType2_CheckedChanged(object sender, EventArgs e)
        {
            AddTitle.Enabled = true;
            AddAlbum.Enabled = false;
            AddStartWork.Enabled = true;
        }

        private void CancelBookButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            Save = true;
            this.Close();
        }
    }
}
