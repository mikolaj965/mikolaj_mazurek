﻿namespace MMazurek_226109_Lab2
{
    partial class AddPositionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PositionType1 = new System.Windows.Forms.RadioButton();
            this.PositionType2 = new System.Windows.Forms.RadioButton();
            this.PositionType3 = new System.Windows.Forms.RadioButton();
            this.PositionType = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.CancelBookButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.AddExt = new System.Windows.Forms.TextBox();
            this.AddFileName = new System.Windows.Forms.TextBox();
            this.Add = new System.Windows.Forms.Button();
            this.AddTitle = new System.Windows.Forms.TextBox();
            this.BookCover = new System.Windows.Forms.GroupBox();
            this.BookCover2 = new System.Windows.Forms.RadioButton();
            this.BookCover1 = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.AddAuthor = new System.Windows.Forms.TextBox();
            this.AddDate = new System.Windows.Forms.NumericUpDown();
            this.AddSize = new System.Windows.Forms.NumericUpDown();
            this.AddLength = new System.Windows.Forms.NumericUpDown();
            this.AddPages = new System.Windows.Forms.NumericUpDown();
            this.PositionType.SuspendLayout();
            this.BookCover.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AddDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddPages)).BeginInit();
            this.SuspendLayout();
            // 
            // PositionType1
            // 
            this.PositionType1.AutoSize = true;
            this.PositionType1.Checked = true;
            this.PositionType1.Location = new System.Drawing.Point(6, 19);
            this.PositionType1.Name = "PositionType1";
            this.PositionType1.Size = new System.Drawing.Size(62, 17);
            this.PositionType1.TabIndex = 0;
            this.PositionType1.TabStop = true;
            this.PositionType1.Text = "Książka";
            this.PositionType1.UseVisualStyleBackColor = true;
            this.PositionType1.CheckedChanged += new System.EventHandler(this.PositionType1_CheckedChanged);
            // 
            // PositionType2
            // 
            this.PositionType2.AutoSize = true;
            this.PositionType2.Location = new System.Drawing.Point(73, 19);
            this.PositionType2.Name = "PositionType2";
            this.PositionType2.Size = new System.Drawing.Size(59, 17);
            this.PositionType2.TabIndex = 1;
            this.PositionType2.TabStop = true;
            this.PositionType2.Text = "Gazeta";
            this.PositionType2.UseVisualStyleBackColor = true;
            this.PositionType2.CheckedChanged += new System.EventHandler(this.PositionType2_CheckedChanged);
            // 
            // PositionType3
            // 
            this.PositionType3.AutoSize = true;
            this.PositionType3.Location = new System.Drawing.Point(139, 19);
            this.PositionType3.Name = "PositionType3";
            this.PositionType3.Size = new System.Drawing.Size(68, 17);
            this.PositionType3.TabIndex = 2;
            this.PositionType3.TabStop = true;
            this.PositionType3.Text = "Nagranie";
            this.PositionType3.UseVisualStyleBackColor = true;
            this.PositionType3.CheckedChanged += new System.EventHandler(this.PositionType3_CheckedChanged);
            // 
            // PositionType
            // 
            this.PositionType.Controls.Add(this.PositionType1);
            this.PositionType.Controls.Add(this.PositionType3);
            this.PositionType.Controls.Add(this.PositionType2);
            this.PositionType.Location = new System.Drawing.Point(12, 12);
            this.PositionType.Name = "PositionType";
            this.PositionType.Size = new System.Drawing.Size(210, 47);
            this.PositionType.TabIndex = 3;
            this.PositionType.TabStop = false;
            this.PositionType.Text = "Rozaj";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 278);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 41;
            this.label5.Text = "Dlugość:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 301);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 40;
            this.label6.Text = "Rozmiar[MB]:";
            // 
            // CancelBookButton
            // 
            this.CancelBookButton.Location = new System.Drawing.Point(13, 329);
            this.CancelBookButton.Name = "CancelBookButton";
            this.CancelBookButton.Size = new System.Drawing.Size(75, 23);
            this.CancelBookButton.TabIndex = 37;
            this.CancelBookButton.Text = "Anuluj";
            this.CancelBookButton.UseVisualStyleBackColor = true;
            this.CancelBookButton.Click += new System.EventHandler(this.CancelBookButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 223);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 36;
            this.label4.Text = "Rozszerzenie:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 252);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 35;
            this.label3.Text = "Nazwa pliku:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 34;
            this.label2.Text = "Rok wydania:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 33;
            this.label1.Text = "Tytuł:";
            // 
            // AddExt
            // 
            this.AddExt.Enabled = false;
            this.AddExt.Location = new System.Drawing.Point(94, 223);
            this.AddExt.Name = "AddExt";
            this.AddExt.Size = new System.Drawing.Size(100, 20);
            this.AddExt.TabIndex = 32;
            // 
            // AddFileName
            // 
            this.AddFileName.Enabled = false;
            this.AddFileName.Location = new System.Drawing.Point(94, 249);
            this.AddFileName.Name = "AddFileName";
            this.AddFileName.Size = new System.Drawing.Size(100, 20);
            this.AddFileName.TabIndex = 31;
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(119, 329);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(75, 23);
            this.Add.TabIndex = 29;
            this.Add.Text = "Dodaj";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // AddTitle
            // 
            this.AddTitle.Location = new System.Drawing.Point(94, 117);
            this.AddTitle.Name = "AddTitle";
            this.AddTitle.Size = new System.Drawing.Size(100, 20);
            this.AddTitle.TabIndex = 28;
            // 
            // BookCover
            // 
            this.BookCover.Controls.Add(this.BookCover2);
            this.BookCover.Controls.Add(this.BookCover1);
            this.BookCover.Location = new System.Drawing.Point(12, 65);
            this.BookCover.Name = "BookCover";
            this.BookCover.Size = new System.Drawing.Size(210, 42);
            this.BookCover.TabIndex = 42;
            this.BookCover.TabStop = false;
            this.BookCover.Text = "Okładka książki";
            // 
            // BookCover2
            // 
            this.BookCover2.AutoSize = true;
            this.BookCover2.Location = new System.Drawing.Point(73, 19);
            this.BookCover2.Name = "BookCover2";
            this.BookCover2.Size = new System.Drawing.Size(61, 17);
            this.BookCover2.TabIndex = 5;
            this.BookCover2.TabStop = true;
            this.BookCover2.Text = "Twarda";
            this.BookCover2.UseVisualStyleBackColor = true;
            this.BookCover2.CheckedChanged += new System.EventHandler(this.BookCover2_CheckedChanged);
            // 
            // BookCover1
            // 
            this.BookCover1.AutoSize = true;
            this.BookCover1.Checked = true;
            this.BookCover1.Location = new System.Drawing.Point(12, 19);
            this.BookCover1.Name = "BookCover1";
            this.BookCover1.Size = new System.Drawing.Size(60, 17);
            this.BookCover1.TabIndex = 6;
            this.BookCover1.TabStop = true;
            this.BookCover1.Text = "Miękka";
            this.BookCover1.UseVisualStyleBackColor = true;
            this.BookCover1.CheckedChanged += new System.EventHandler(this.BookCover1_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 175);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 44;
            this.label7.Text = "Ilość stron:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 201);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 46;
            this.label8.Text = "Autor:";
            // 
            // AddAuthor
            // 
            this.AddAuthor.Location = new System.Drawing.Point(94, 194);
            this.AddAuthor.Name = "AddAuthor";
            this.AddAuthor.Size = new System.Drawing.Size(100, 20);
            this.AddAuthor.TabIndex = 45;
            // 
            // AddDate
            // 
            this.AddDate.Location = new System.Drawing.Point(94, 143);
            this.AddDate.Maximum = new decimal(new int[] {
            2016,
            0,
            0,
            0});
            this.AddDate.Minimum = new decimal(new int[] {
            1900,
            0,
            0,
            0});
            this.AddDate.Name = "AddDate";
            this.AddDate.Size = new System.Drawing.Size(100, 20);
            this.AddDate.TabIndex = 69;
            this.AddDate.Value = new decimal(new int[] {
            1900,
            0,
            0,
            0});
            // 
            // AddSize
            // 
            this.AddSize.DecimalPlaces = 3;
            this.AddSize.Enabled = false;
            this.AddSize.Location = new System.Drawing.Point(94, 299);
            this.AddSize.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.AddSize.Name = "AddSize";
            this.AddSize.Size = new System.Drawing.Size(100, 20);
            this.AddSize.TabIndex = 75;
            // 
            // AddLength
            // 
            this.AddLength.DecimalPlaces = 2;
            this.AddLength.Enabled = false;
            this.AddLength.Location = new System.Drawing.Point(94, 273);
            this.AddLength.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.AddLength.Name = "AddLength";
            this.AddLength.Size = new System.Drawing.Size(100, 20);
            this.AddLength.TabIndex = 74;
            // 
            // AddPages
            // 
            this.AddPages.Location = new System.Drawing.Point(94, 168);
            this.AddPages.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.AddPages.Name = "AddPages";
            this.AddPages.Size = new System.Drawing.Size(100, 20);
            this.AddPages.TabIndex = 76;
            // 
            // AddPositionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(236, 360);
            this.ControlBox = false;
            this.Controls.Add(this.AddPages);
            this.Controls.Add(this.AddSize);
            this.Controls.Add(this.AddLength);
            this.Controls.Add(this.AddDate);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.AddAuthor);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.BookCover);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.CancelBookButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AddExt);
            this.Controls.Add(this.AddFileName);
            this.Controls.Add(this.Add);
            this.Controls.Add(this.AddTitle);
            this.Controls.Add(this.PositionType);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "AddPositionForm";
            this.Text = "AddPositionForm";
            this.PositionType.ResumeLayout(false);
            this.PositionType.PerformLayout();
            this.BookCover.ResumeLayout(false);
            this.BookCover.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AddDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddPages)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton PositionType1;
        private System.Windows.Forms.RadioButton PositionType2;
        private System.Windows.Forms.RadioButton PositionType3;
        private System.Windows.Forms.GroupBox PositionType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button CancelBookButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox AddExt;
        private System.Windows.Forms.TextBox AddFileName;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.TextBox AddTitle;
        private System.Windows.Forms.GroupBox BookCover;
        private System.Windows.Forms.RadioButton BookCover2;
        private System.Windows.Forms.RadioButton BookCover1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox AddAuthor;
        private System.Windows.Forms.NumericUpDown AddDate;
        private System.Windows.Forms.NumericUpDown AddSize;
        private System.Windows.Forms.NumericUpDown AddLength;
        private System.Windows.Forms.NumericUpDown AddPages;
    }
}