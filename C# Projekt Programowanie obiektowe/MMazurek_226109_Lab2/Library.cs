﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Windows.Forms;

namespace MMazurek_226109_Lab2
{
    class Library
    {
        private static int ID =0;
        private static int UserID=0;
        protected List<Position> poz = new List<Position>();
        protected List<User> user = new List<User>();


        public Library()
        {
            this.importDB();
        }

        public static int getNextID()
        {
            return ID+1;
        }

        public void showUser(ListView obj)
        {
            for (int i = 0; i < user.Count; i++)
            {
                user[i].showPosition(obj);
            }
        }

        public static void enlargeID()
        {
            ID++;
        }

        public static void enlargeUserID()
        {
            UserID++;
        }

        public static int getNextUserID()
        {
            return UserID+1;
        }

        public void AddPosition(ListView list)
        {
            poz.Add(new Position().AddPosition(list));
        }

        public void UpdateVector(int id)
        {
            for (int i = 0; i < poz.Count; i++)
            {
                if (poz[i].getPositionID() == id)
                {
                    poz.RemoveAt(i);
                }
            }
        }

        public void deletePosition(int id, ListView list)
        {
            for (int i = 0; i < poz.Count; i++)
            {
                if (poz[i].getPositionID() == id)
                {
                    ListViewItem item = list.FindItemWithText(id.ToString());
                    item.Remove();
                    poz.RemoveAt(i);
                }
            }
        }

        public void GenerateXML()
        {
            XmlTextWriter writer = new XmlTextWriter("database.xml", System.Text.Encoding.UTF8);
            writer.WriteStartDocument(true);
            writer.Formatting = Formatting.Indented;
            writer.Indentation = 2;
                writer.WriteStartElement("library");
                    writer.WriteStartElement("users");
            
                    foreach (User element in user)
                    {
                         element.generateXML(writer);
                    }

                    writer.WriteEndElement();

                    writer.WriteStartElement("positions");

                    foreach (Position element in poz)
                    {
                        element.generateXML(writer);
                    }

                writer.WriteEndElement();
            writer.WriteEndElement();
            
            writer.WriteEndDocument();
            writer.Close();
        }


        public void editPosition(int id, ListView list)
        {
            foreach (Position element in poz)
            {
                if (element.getPositionID() == id)
                {
                    element.editPosition(element, list);
                }
            }
        }

        public void AddUser(ListView list)
        {
            user.Add(new User().AddUser(list));
        }

        public void deleteUser(int id, ListView list)
        {
            for (int i = 0; i < user.Count; i++)
            {
                if (user[i].getUserID() == id)
                {
                    ListViewItem item = list.FindItemWithText(id.ToString());
                    item.Remove();
                    user.RemoveAt(i);
                }
            }
        }

        public bool CheckUserPos(int uid)
        {
            int count = 0;
            for (int i = 0; i < poz.Count; i++)
            {
                if (poz[i].GetUserID() == uid)
                {
                    count++;
                }
            }
            return (count < 3 ? true : false);
        }

        public bool GetUserType(int uid)
        {
            for (int i = 0; i < user.Count; i++)
            {
                if (user[i].getUserID() == uid)
                {
                    return user[i].GetUserType();
                }
            }
            return false;
        }

        public bool CheckAvailable(int id)
        {
            for (int i = 0; i < poz.Count; i++)
            {
                if (poz[i].getPositionID() == id && poz[i].GetUserID() == 0)
                {
                    return true;
                }
            }
            return false;
        }

        public string GivePos(int uid, int pid, string maxDate)
        {
            string userLogin = "";
            for (int i = 0; i < user.Count; i++)
            {
                if (user[i].getUserID() == uid)
                {
                    userLogin = user[i].GetUserLogin();
                }
            }
            for (int i = 0; i < poz.Count; i++)
            {
                if (poz[i].getPositionID() == pid)
                {
                    poz[i].givePos(uid, userLogin, maxDate);
                }
            }
            return userLogin;
        }


        public void showPosition(ListView obj)
        {
            for (int i=0; i<poz.Count; i++)
            {
                poz[i].showPosition(obj);
            }
        }

        private void importDB() //uprościć
        {
            XmlDocument XmlDoc = new XmlDocument();
            try
            {
                XmlDoc.Load("database.xml");
                XmlNodeList node = XmlDoc.GetElementsByTagName("book");
                for (int i=0; i<node.Count; i++)
                {
                    try {
                        int id = int.Parse(node[i].ChildNodes.Item(0).InnerText.Trim());
                        int uid = int.Parse(node[i].ChildNodes.Item(1).InnerText.Trim());
                        string title = node[i].ChildNodes.Item(2).InnerText.Trim();
                        int date = int.Parse(node[i].ChildNodes.Item(3).InnerText.Trim());
                        int pages = int.Parse(node[i].ChildNodes.Item(4).InnerText.Trim());
                        bool cover = Convert.ToBoolean(int.Parse(node[i].ChildNodes.Item(5).InnerText.Trim()));
                        string author = node[i].ChildNodes.Item(6).InnerText.Trim();
                        string userLogin = node[i].ChildNodes.Item(7).InnerText.Trim();
                        string maxDate = node[i].ChildNodes.Item(8).InnerText.Trim();

                        poz.Add(new Book(id, uid, false, title, date, pages, cover, author, userLogin, maxDate));
                        if (id > ID)
                            ID = id;
                    }
                    catch
                    {

                    }
                }

                node = XmlDoc.GetElementsByTagName("recording");
                for (int i = 0; i < node.Count; i++)
                {
                    int id = int.Parse(node[i].ChildNodes.Item(0).InnerText.Trim());
                    int uid = int.Parse(node[i].ChildNodes.Item(1).InnerText.Trim());
                    string title = node[i].ChildNodes.Item(2).InnerText.Trim();
                    int date = int.Parse(node[i].ChildNodes.Item(3).InnerText.Trim());
                    string ext = node[i].ChildNodes.Item(4).InnerText.Trim();
                    string filename = node[i].ChildNodes.Item(5).InnerText.Trim();
                    float length = float.Parse(node[i].ChildNodes.Item(6).InnerText.Trim());
                    float size = float.Parse(node[i].ChildNodes.Item(7).InnerText.Trim());
                    string userLogin = node[i].ChildNodes.Item(8).InnerText.Trim();
                    string maxDate = node[i].ChildNodes.Item(9).InnerText.Trim();

                    poz.Add(new Recording(id, uid, false, title, date, ext, filename, length, size, userLogin, maxDate));
                    if (id > ID)
                        ID = id;
                }


                node = XmlDoc.GetElementsByTagName("newspaper");
                
                for (int i = 0; i < node.Count; i++)
                {
                    try {
                        int id = int.Parse(node[i].ChildNodes.Item(0).InnerText.Trim());
                        int uid = int.Parse(node[i].ChildNodes.Item(1).InnerText.Trim());
                        string title = node[i].ChildNodes.Item(2).InnerText.Trim();
                        int date = int.Parse(node[i].ChildNodes.Item(3).InnerText.Trim());
                        int pages = int.Parse(node[i].ChildNodes.Item(4).InnerText.Trim());
                        string userLogin = node[i].ChildNodes.Item(5).InnerText.Trim();
                        string maxDate = node[i].ChildNodes.Item(6).InnerText.Trim();

                        poz.Add(new Newspaper(id, uid, false, title, date, pages, userLogin, maxDate));
                        if (id > ID)
                            ID = id;
                    }
                    catch
                    {

                    }
                }

                node = XmlDoc.GetElementsByTagName("student");
                for (int i = 0; i < node.Count; i++)
                {
                    int id = int.Parse(node[i].ChildNodes.Item(0).InnerText.Trim());
                    string name = node[i].ChildNodes.Item(1).InnerText.Trim();
                    string surname = node[i].ChildNodes.Item(2).InnerText.Trim();
                    int date = int.Parse(node[i].ChildNodes.Item(3).InnerText.Trim());
                    bool gender = Convert.ToBoolean(int.Parse(node[i].ChildNodes.Item(4).InnerText.Trim()));
                    int album = Convert.ToInt32(node[i].ChildNodes.Item(5).InnerText.Trim());
                    int books = int.Parse(node[i].ChildNodes.Item(6).InnerText.Trim());

                    user.Add(new Student(id, false, name, surname, date, gender, album, books));
                    if (id > UserID)
                        UserID = id;
                }
                
                node = XmlDoc.GetElementsByTagName("teacher");
                for (int i = 0; i < node.Count; i++)
                {
                    int id = int.Parse(node[i].ChildNodes.Item(0).InnerText.Trim());
                    string name = node[i].ChildNodes.Item(1).InnerText.Trim();
                    string surname = node[i].ChildNodes.Item(2).InnerText.Trim();
                    int date = int.Parse(node[i].ChildNodes.Item(3).InnerText.Trim());
                    bool gender = Convert.ToBoolean(int.Parse(node[i].ChildNodes.Item(4).InnerText.Trim()));
                    int books = int.Parse(node[i].ChildNodes.Item(5).InnerText.Trim());
                    string title = node[i].ChildNodes.Item(6).InnerText.Trim();
                    string startWork = node[i].ChildNodes.Item(7).InnerText.Trim();

                    user.Add(new Teacher(id, true, name, surname, date, gender, books, startWork, title));
                    if (id > UserID)
                        UserID = id;
                }

            }
            catch (XmlException e)
            {
                MessageBox.Show(e.Message);
            }
        }


        ~Library()
        {
            this.GenerateXML();
        }
    }
}