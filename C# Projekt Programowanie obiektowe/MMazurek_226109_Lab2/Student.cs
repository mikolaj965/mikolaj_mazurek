﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace MMazurek_226109_Lab2
{
    class Student : User
    {
        private int album;

        public Student(int id, bool type, string name, string surname, int date, bool gender, int album, int books)
        {
            this.id = id;
            this.type = type;
            this.name = name;
            this.surname = surname;
            this.date = date;
            this.gender = gender;
            this.album = album;
            this.books = books;
        }
        public override void showPosition(ListView obj)
        {
            ListViewItem item = new ListViewItem(id.ToString());
            item.SubItems.Add("Student");
            item.SubItems.Add("-");
            item.SubItems.Add(name);
            item.SubItems.Add(surname);
            item.SubItems.Add(date.ToString());
            item.SubItems.Add(((gender == false) ? "M" : "K"));
            item.SubItems.Add(album.ToString());
            item.SubItems.Add("-");
            obj.Items.Add(item);
        }
        public override void generateXML(XmlTextWriter writer)
        {
            writer.WriteStartElement("student");
                writer.WriteStartElement("id");
                    writer.WriteString(id.ToString());
                writer.WriteEndElement();
            
                writer.WriteStartElement("name");
                    writer.WriteString(name);
                writer.WriteEndElement();

                writer.WriteStartElement("surname");
                    writer.WriteString(surname);
                writer.WriteEndElement();

                writer.WriteStartElement("date");
                    writer.WriteString(date.ToString());
                writer.WriteEndElement();
            
                writer.WriteStartElement("gender");
                    writer.WriteString(Convert.ToInt16(gender).ToString());
                writer.WriteEndElement();
            
                writer.WriteStartElement("album");
                    writer.WriteString(album.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("books");
                    writer.WriteString(books.ToString());
                writer.WriteEndElement();
            writer.WriteEndElement();
        }
    }
}
